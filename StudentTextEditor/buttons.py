from PIL import ImageTk, Image

bullet1 = ImageTk.PhotoImage(Image.open('Buttons/bullet1.jpeg'))
bullet2 = ImageTk.PhotoImage(Image.open('Buttons/bullet2.jpeg'))
bullet3 = ImageTk.PhotoImage(Image.open('Buttons/bullet3.jpeg'))

number1 = ImageTk.PhotoImage(Image.open('Buttons/number1.jpeg'))
number2 = ImageTk.PhotoImage(Image.open('Buttons/number2.jpeg'))
number3 = ImageTk.PhotoImage(Image.open('Buttons/number3.jpeg'))

left1 = ImageTk.PhotoImage(Image.open('Buttons/left1.jpeg'))
left2 = ImageTk.PhotoImage(Image.open('Buttons/left2.jpeg'))
left3 = ImageTk.PhotoImage(Image.open('Buttons/left3.jpeg'))

center1 = ImageTk.PhotoImage(Image.open('Buttons/center1.jpeg'))
center2 = ImageTk.PhotoImage(Image.open('Buttons/center2.jpeg'))
center3 = ImageTk.PhotoImage(Image.open('Buttons/center3.jpeg'))

right1 = ImageTk.PhotoImage(Image.open('Buttons/right1.jpeg'))
right2 = ImageTk.PhotoImage(Image.open('Buttons/right2.jpeg'))
right3 = ImageTk.PhotoImage(Image.open('Buttons/right3.jpeg'))

lines1 = ImageTk.PhotoImage(Image.open('Buttons/lines1.jpeg'))
lines2 = ImageTk.PhotoImage(Image.open('Buttons/lines2.jpeg'))
lines3 = ImageTk.PhotoImage(Image.open('Buttons/lines3.jpeg'))

para1 = ImageTk.PhotoImage(Image.open('Buttons/para1.jpeg'))
para2 = ImageTk.PhotoImage(Image.open('Buttons/para2.jpeg'))
para3 = ImageTk.PhotoImage(Image.open('Buttons/para3.jpeg'))

highlight1None = ImageTk.PhotoImage(Image.open('Buttons/highlight1None.jpeg'))
highlight2None = ImageTk.PhotoImage(Image.open('Buttons/highlight2None.jpeg'))
highlight3None = ImageTk.PhotoImage(Image.open('Buttons/highlight3None.jpeg'))

highlight3any = ImageTk.PhotoImage(Image.open('Buttons/highlight3any.jpeg'))

highlight1_000000 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#000000.jpeg'))
highlight2_000000 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#000000.jpeg'))

highlight1_0000FF = ImageTk.PhotoImage(Image.open('Buttons/highlight1#0000FF.jpeg'))
highlight2_0000FF = ImageTk.PhotoImage(Image.open('Buttons/highlight2#0000FF.jpeg'))

highlight1_00FF00 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#00FF00.jpeg'))
highlight2_00FF00 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#00FF00.jpeg'))

highlight1_00FFFF = ImageTk.PhotoImage(Image.open('Buttons/highlight1#00FFFF.jpeg'))
highlight2_00FFFF = ImageTk.PhotoImage(Image.open('Buttons/highlight2#00FFFF.jpeg'))

highlight1_4B0082 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#4B0082.jpeg'))
highlight2_4B0082 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#4B0082.jpeg'))

highlight1_8B0000 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#8B0000.jpeg'))
highlight2_8B0000 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#8B0000.jpeg'))

highlight1_006400 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#006400.jpeg'))
highlight2_006400 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#006400.jpeg'))

highlight1_6495ED = ImageTk.PhotoImage(Image.open('Buttons/highlight1#6495ED.jpeg'))
highlight2_6495ED = ImageTk.PhotoImage(Image.open('Buttons/highlight2#6495ED.jpeg'))

highlight1_222222 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#222222.jpeg'))
highlight2_222222 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#222222.jpeg'))

highlight1_800020 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#800020.jpeg'))
highlight2_800020 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#800020.jpeg'))

highlight1_800080 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#800080.jpeg'))
highlight2_800080 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#800080.jpeg'))

highlight1_808080 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#808080.jpeg'))
highlight2_808080 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#808080.jpeg'))

highlight1_CCCC00 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#CCCC00.jpeg'))
highlight2_CCCC00 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#CCCC00.jpeg'))

highlight1_D3D3D3 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#D3D3D3.jpeg'))
highlight2_D3D3D3 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#D3D3D3.jpeg'))

highlight1_EE7600 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#EE7600.jpeg'))
highlight2_EE7600 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#EE7600.jpeg'))

highlight1_FF0000 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#FF0000.jpeg'))
highlight2_FF0000 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#FF0000.jpeg'))

highlight1_FF00FF = ImageTk.PhotoImage(Image.open('Buttons/highlight1#FF00FF.jpeg'))
highlight2_FF00FF = ImageTk.PhotoImage(Image.open('Buttons/highlight2#FF00FF.jpeg'))

highlight1_FFA500 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#FFA500.jpeg'))
highlight2_FFA500 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#FFA500.jpeg'))

highlight1_FFFF00 = ImageTk.PhotoImage(Image.open('Buttons/highlight1#FFFF00.jpeg'))
highlight2_FFFF00 = ImageTk.PhotoImage(Image.open('Buttons/highlight2#FFFF00.jpeg'))

highlight1_FFFFFF = ImageTk.PhotoImage(Image.open('Buttons/highlight1#FFFFFF.jpeg'))
highlight2_FFFFFF = ImageTk.PhotoImage(Image.open('Buttons/highlight2#FFFFFF.jpeg'))


bullets = (bullet1, bullet2, bullet3)
numbers = (number1, number2, number3)
lefts = (left1, left2, left3)
centers = (center1, center2, center3)
rights = (right1, right2, right3)
lines = (lines1, lines2, lines3)
paras = (para1, para2, para3)


highlightDict = {}
# highlightDict[''] = (highlight1None, highlight2None, highlight3None)
highlightDict['NoColor'] = (highlight1None, highlight2None, highlight3None)
highlightDict['#000000'] = (highlight1_000000, highlight2_000000, highlight3any)
highlightDict['#222222'] = (highlight1_222222, highlight2_222222, highlight3any)
highlightDict['#808080'] = (highlight1_808080, highlight2_808080, highlight3any)
highlightDict['#D3D3D3'] = (highlight1_D3D3D3, highlight2_D3D3D3, highlight3any)
highlightDict['#FFFFFF'] = (highlight1_FFFFFF, highlight2_FFFFFF, highlight3any)
highlightDict['#800020'] = (highlight1_800020, highlight2_800020, highlight3any)
highlightDict['#FF0000'] = (highlight1_FF0000, highlight2_FF0000, highlight3any)
highlightDict['#FFA500'] = (highlight1_FFA500, highlight2_FFA500, highlight3any)
highlightDict['#00FF00'] = (highlight1_00FF00, highlight2_00FF00, highlight3any)
highlightDict['#4B0082'] = (highlight1_4B0082, highlight2_4B0082, highlight3any)
highlightDict['#8B0000'] = (highlight1_8B0000, highlight2_8B0000, highlight3any)
highlightDict['#EE7600'] = (highlight1_EE7600, highlight2_EE7600, highlight3any)
highlightDict['#CCCC00'] = (highlight1_CCCC00, highlight2_CCCC00, highlight3any)
highlightDict['#006400'] = (highlight1_006400, highlight2_006400, highlight3any)
highlightDict['#00FFFF'] = (highlight1_00FFFF, highlight2_00FFFF, highlight3any)
highlightDict['#6495ED'] = (highlight1_6495ED, highlight2_6495ED, highlight3any)
highlightDict['#0000FF'] = (highlight1_0000FF, highlight2_0000FF, highlight3any)
highlightDict['#800080'] = (highlight1_800080, highlight2_800080, highlight3any)
highlightDict['#FFFF00'] = (highlight1_FFFF00, highlight2_FFFF00, highlight3any)
highlightDict['#FF00FF'] = (highlight1_FF00FF, highlight2_FF00FF, highlight3any)

# if __name__ == "__main__":
#   main()