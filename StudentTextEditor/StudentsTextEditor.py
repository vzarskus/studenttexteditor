import os
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import PhotoImage
from itertools import count
from datetime import datetime
import pyperclip
import copy
import docx
from data import * # Import the saved data


class Application:

    def __init__(self, root):

        import buttons as bt
        ''' This module imports and uses ImageTk and Image PIL modules which need
        an active Tk mainloop in order to run which is why this "buttons" module is being imported inside the mainloop.'''
        self.mainframe = ttk.Frame(root)
        self.main_one = tk.Frame(self.mainframe, background='white')
        self.main_two = ttk.Frame(self.mainframe)
        self.main_three = ttk.Frame(self.mainframe)
####################
        self.text = ImprovedText(self.main_three, relief='flat', wrap='word', tabs='17m',
            font=['Calibri', 12], selectbackground='SystemHighlight', inactiveselectbackground='SystemHighlight', height=30)
        self.scrollbar_text = ttk.Scrollbar(self.main_three, orient='vertical',
            command=self.text.yview)
        self.text['yscrollcommand'] = self.scrollbar_text.set
        self.subject_menu = SubjectMenu(self.main_two, func=self.change_subject, caller=self)
        self.lecture_menu = LectureMenu(self.main_two, caller=self)
#################
        self.dropdown_font = DropdownFont(self.main_one, func=self.font)
        self.button_font = self.dropdown_font.button
        self.dropdown_size = DropdownSize(self.main_one, func=self.size, textwidget=self.text)
        self.button_size = self.dropdown_size.button_dropdown

        self.button_bold = ButtonStyle(self.main_one, func=lambda: self.b_i_u('bold'), text="B", font='Calibri 15 bold')
        self.button_italic = ButtonStyle(self.main_one, func=lambda: self.b_i_u('italic'), text="I", font='Calibri 15 italic')
        self.button_underline = ButtonStyle(self.main_one, func=lambda: self.b_i_u(underline=True), text="U", font='Calibri 15 underline')

        self.button_color = ButtonColor(self.main_one, func=self.color, text="A", font='Calibri 15 bold underline')
        self.button_highlight = ButtonHighlight(self.main_one, images=bt.highlightDict['NoColor'], func=self.highlight, color_dict=bt.highlightDict)

        self.button_left = ButtonJust(self.main_one, images=bt.lefts, func=lambda: self.justify('left'))
        self.button_center = ButtonJust(self.main_one, images=bt.centers, func=lambda: self.justify('center'))
        self.button_right = ButtonJust(self.main_one, images=bt.rights, func=lambda: self.justify('right'))
        self.button_left.otherbuttons(self.button_center, self.button_right)
        self.button_center.otherbuttons(self.button_left, self.button_right)
        self.button_right.otherbuttons(self.button_left, self.button_center)

        self.button_lines = ButtonSpacing(self.main_one, type_='lines', images=bt.lines, func=lambda: self.spacing('lines'))
        self.button_paragraphs = ButtonSpacing(self.main_one, type_='paragraphs', images=bt.paras, func=lambda: self.spacing('paragraphs'))

        self.button_bullet = ButtonListing(self.main_one, images=bt.bullets, func=lambda: self.indent('bullet'))
        self.button_number = ButtonListing(self.main_one, images=bt.numbers, func=lambda: self.indent('number'))

#################################### GEO MANAGERS
        self.mainframe.pack(expand=True, fill='both', padx=(5, 0), pady=5)
        self.main_one.pack(fill='both', padx=(0, 5))
        self.main_two.pack(fill='both', pady=(5, 0), padx=(0, 5))
        self.main_three.pack(expand=True, fill='both', pady=(5, 10))
#####################
        self.dropdown_font.pack(side='left', expand=True, fill='x')
        self.dropdown_size.pack(side='left', expand=True, fill='x')
        self.button_bold.pack(side='left', expand=True, fill='x')
        self.button_italic.pack(side='left', expand=True, fill='x')
        self.button_underline.pack(side='left', expand=True, fill='x')
        self.button_color.pack(side='left', expand=True, fill='x')
        self.button_highlight.pack(side='left', expand=True, fill='x')
        self.button_left.pack(side='left', expand=True, fill='x')
        self.button_center.pack(side='left', expand=True, fill='x')
        self.button_right.pack(side='left', expand=True, fill='x')
        self.button_lines.pack(side='left', expand=True, fill='x')
        self.button_paragraphs.pack(side='left', expand=True, fill='x')
        self.button_bullet.pack(side='left', expand=True, fill='x')
        self.button_number.pack(side='left', expand=True, fill='x')
#######################
        self.subject_menu.pack(side='left', expand=True, anchor='w',padx=(0, 5))
        self.lecture_menu.pack(side='left', expand=True, fill='x')
        self.text.grid(column=0, row=0, sticky='NSEW')
        self.scrollbar_text.grid(column=1, row=0, sticky='N, S')
        self.main_three.grid_rowconfigure(0, weight=1)         
        self.main_three.grid_columnconfigure(0, weight=1)
        # I was unable to get the desired resizing with the pack() geo manager for main_three.

########################## BINDINGS
        root.bind('<Control-z>', lambda e: self.load(direction='undo'))
        root.bind('<Control-Z>', lambda e: self.load(direction='redo'))
        root.bind('<Control-y>', lambda e: self.load(direction='redo'))

        self.text.bind('<<TextInsert>>', self.writing)
        self.text.bind('<<TextDelete>>', self.deletion)
        self.text.bind('<<CursorChange>>', self.cursor)
        self.text.bind('<Control-c>', self.copy)
        self.text.bind('<Control-C>', self.copy)
        self.text.bind('<Control-x>', self.cut)
        self.text.bind('<Control-X>', self.cut)
        self.text.bind('<Control-v>', self.paste)
        self.text.bind('<Control-V>', self.paste)

        self.text.bind('<Control-b>', self.button_bold.press)
        self.text.bind('<Control-B>', self.button_bold.press)
        self.text.bind('<Control-i>', self.button_italic.press)
        self.text.bind('<Control-I>', self.button_italic.press)
        self.text.bind('<Control-u>', self.button_underline.press)
        self.text.bind('<Control-U>', self.button_underline.press)

########################### CONTEXT MENUS
        root.option_add('*tearOff', 'false')
        root_menu = tk.Menu(root)
        root.config(menu=root_menu)
        menu_file = tk.Menu(root_menu)
        root_menu.add_cascade(menu=menu_file, label='File')
        download = tk.Menu(menu_file)
        menu_file.add_cascade(menu=download, label='Download as Word document')
        download.add_command(label='Current lecture', command=lambda: self.download('lecture'))
        download.add_command(label='Current subject', command=lambda: self.download('subject'))
        download.add_command(label='Everything', command=lambda: self.download('everything'))
        menu_file.add_command(label='Reset', command=self.reset)
        menu_file.add_command(label='Exit', command=root.destroy)
        

        self.context_subject = tk.Menu(self.subject_menu)
        self.context_subject.add_command(label="Rename", command=lambda: self.rename(menu=self.subject_menu))
        self.context_subject.add_command(label="Delete", command=self.subject_menu.delete)
        context_subject2 = tk.Menu(self.context_subject)
        self.context_subject.add_cascade(menu=context_subject2, label="Sort by")
        context_subject2.add_command(label="Date created", command=lambda: self.subject_menu.sort('date_created', save=True))
        context_subject2.add_command(label="Name", command=lambda: self.subject_menu.sort('name', save=True))
        self.subject_menu.win.bind('<3>', self.post_subject)

        self.context_lecture = tk.Menu(self.lecture_menu)
        self.context_lecture.add_command(label="Rename", command=lambda: self.rename(menu=self.lecture_menu))
        self.context_lecture.add_command(label="Delete", command=lambda: self.lecture_menu.delete())

        self.context_text = tk.Menu(self.text)
        self.context_text.add_command(label='Bold',  command=self.button_bold.press)
        self.context_text.add_command(label='Italic', command=self.button_italic.press)
        self.context_text.add_separator()
        self.context_text.add_command(label='Cut', command=self.cut)
        self.context_text.add_command(label='Copy', command=self.copy)
        self.context_text.add_command(label='Paste', command=self.paste)
        self.context_text.add_command(label='Delete', command=self.delete)
        self.context_text.add_command(label='Undo', command=lambda: self.load('undo'))
        self.context_text.add_command(label='Redo', command=lambda: self.load('redo'))
        self.text.bind('<3>', lambda e: self.context_text.post(e.x_root, e.y_root))

#################### OTHER 
        root.maxsize(width=1000, height=root.winfo_screenheight())
        # Unfortunately, Tkinter is not behaving very well on full screen. In my case I would need to change Subject Menu from a button that calls the Popupwindow
        # to the Frame that appears near the Text which would take space and be as a reference for where the Popupwindow should be. However, adding a new widget 
        # in full-screen mode crashes the application with the python stackoverflow error, thus not giving me any valuable information on what could be improved.
        self.changed_configs = False
        self.local_clipboard = False
        self.clipboard_tags = {}
        # For managing the clipboard I use pyperclip module plus my own machine (to manage tags).
        # For anyone wondering, Tk's built in clipboard manager is super buggy.

        self.all_tag_configs = ['font', 'justify', 'underline', 'foreground', 'background', 'spacing2', 'spacing1', 'spacing3', 'lmargin1', 'lmargin2']

#################################### TEXT EDITING ENGINE FUNCTIONS

    def writing(self, *args):

        '''Function that deals with regular writing and changing configurations while writing.'''

        tag = self.text.tag_names('insert-2c')
        if tag == ():
        # Tkinter should always apply the tag that is on the left of the
        # just inserted char. However, if there is no tag to get on the left 
        # add tag which is on the right instead.
            tag = self.text.tag_names('insert')
            if tag == (): 
            # In rare cases (I have found one, when the whole text gets deleted), all
            # the tags are gone so we need to create a default one.
                tag = next(self.yield_tag())
                self.text.tag_configure(tag, font=['Calibri', 12], justify='left', foreground='#000000', background='', underline=0, spacing2=0, spacing1=0, spacing3=0, lmargin1=0, lmargin2=0)
            self.text.tag_add(tag, 'insert-1c')
        tag = self.text.tag_names('insert-1c')
        if tag == ():
        # If Tk has failed to apply the tag automatically (from the ch on the left), 
        # let's do it manually.
            tag = self.text.tag_names('insert-2c')
            self.text.tag_add(tag, 'insert-1c')

        if self.changed_configs:
        # If a new configuration has been set by the user, we need the
        # newly inserted text to have it.
            self.text.tag_remove(tag, 'insert-1c', 'insert')
            subject_id = self.subject_menu.get()
            newtag = next(self.yield_tag())
            self.text.tag_add(newtag, 'insert-1c')

            almost_all_configs = [tag for tag in self.all_tag_configs if tag != 'font' if tag != 'underline' if tag != 'foreground' if tag != 'background']
            for config in almost_all_configs:
                self.text.tag_configure(newtag, **{config: self.text.tag_cget(tag, config)})

            font = [self.button_font.get()]
            font.append(self.button_size.get())
            if self.button_bold.pressed:
                font.append('bold')
            if self.button_italic.pressed:
                font.append('italic')
            self.text.tag_configure(newtag, font=font)
                
            if self.button_underline.pressed:
                self.text.tag_configure(newtag, underline=1)
            else:
                self.text.tag_configure(newtag, underline=0)

            self.text.tag_configure(newtag, foreground=self.button_color.get(), background=self.button_highlight.get())

            self.changed_configs = False

        self.text.see('insert')
        self.save()


    def deletion(self, *args):
        
        tag = self.text.tag_names('insert' + ' linestart')
        if self.text.tag_cget(tag, 'lmargin1') != '0':
            ''' The first part checks if bulleting and/or numbering have been deleted, so the indentation
             should be removed'''
            if self.text.get(self.text.index('insert'+ ' linestart +1c')) != '•' and self.text.get(self.text.index('insert'+ ' linestart +1c')) not in [num for num in '0123456789']:
                self.text.tag_add('sel', self.text.index('insert') +' linestart -1c', self.text.index('insert') +' lineend +1c')
                self.config_selection('lmargin1', 'adjust', '0')
                self.config_selection('lmargin2', 'adjust', '0')
                self.text.tag_remove('sel', 1.0, 'end')
                self.text.delete('insert'+ ' linestart')
                # We delete the space in the end as well for that smooth feel.

        tag = self.text.tag_names('insert')
        if self.text.get('insert') != '\n':
            ''' This part deals with the fact that when deleted text merges with the previous line,
            it may not have the same indentation/spacing/justification parameters so that needs to be adjusted.'''
            line_tag = self.text.tag_names('insert' + ' linestart')
            needs_adjustment = False
            almost_all_configs = [tag for tag in self.all_tag_configs if tag != 'font' if tag != 'underline' if tag != 'foreground' if tag != 'background']
            for config in almost_all_configs:
                if self.text.tag_cget(tag, config) != self.text.tag_cget(line_tag, config):
                    needs_adjustment = True
                    break
            if needs_adjustment:
                self.text.tag_add('sel', 'insert', 'insert' +' lineend')
                self.config_selection('justify', self.text.tag_cget(line_tag, 'justify'))
                self.config_selection('spacing2', self.text.tag_cget(line_tag, 'spacing2'))
                self.config_selection('spacing1', self.text.tag_cget(line_tag, 'spacing1'))
                self.config_selection('spacing3', self.text.tag_cget(line_tag, 'spacing3'))
                self.config_selection('lmargin1', 'adjust', self.text.tag_cget(line_tag, 'lmargin1'))
                self.config_selection('lmargin2', 'adjust', self.text.tag_cget(line_tag, 'lmargin2'))
                self.text.tag_remove('sel', 1.0, 'end')
                self.cursor()   # To set widgets.
    

    def cursor(self, *args):

        ''' Sets the configurations of the widgets in the frame_one according
         to the position of the "insert" mark/cursor.'''

        if self.selection_in_progress():
        # If the selection is in progress, widgets in frame one will 
        # show only those configurations that all the chars in the 
        # selected text share, otherwise the widget will be blank/unselected/not pressed.
            config_dic = {'size': [], 'bold': [], 'italic': []}
            # These configs are not in all_tag_configs as they all go under
            # "font", however, we need to separate them.
            for tagconfig in self.all_tag_configs:
                config_dic[tagconfig] = []
                # Make other initial values as empty lists as well.
            tag_dic = self.dump_selection()
            for tag in tag_dic:
                for tagconfig in self.all_tag_configs:
                    config = self.text.tag_cget(tag, tagconfig)
                    if tagconfig == 'font':
                        font = font_unpack(config)
                        config_dic['font'].append(font[0])
                        config_dic['size'].append(font[1])
                        if 'bold' in font:
                            config_dic['bold'].append(True)
                        else:
                            config_dic['bold'].append(False)
                        if 'italic' in font:
                            config_dic['italic'].append(True)
                        else:
                            config_dic['italic'].append(False)
                    else:
                        config_dic[tagconfig].append(config)

            if len(set(config_dic['font'])) == 1:
                self.button_font.set(config_dic['font'][0])
            else:
                self.button_font.set('')
            if len(set(config_dic['size'])) == 1:
                self.button_size.set(config_dic['size'][0])             
            else:
                self.button_size.set('multiple')

            if (len(set(config_dic['bold'])) == 1 and
                True in config_dic['bold']):
                    self.button_bold.press(set_state='pressed')
            else:
                self.button_bold.press(set_state='!pressed')
            if (len(set(config_dic['italic'])) == 1 and 
                True in config_dic['italic']):
                    self.button_italic.press(set_state='pressed')
            else:
                self.button_italic.press(set_state='!pressed')

            if (len(set(config_dic['underline'])) == 1 and 
                '1' in config_dic['underline']):
                    self.button_underline.press(set_state='pressed')
            else:
                self.button_underline.press(set_state='!pressed')

            if len(set(config_dic['justify'])) == 1:
                if 'left' in config_dic['justify']:
                    self.button_left.press(run_func=False)
                elif 'center' in config_dic['justify']:
                    self.button_center.press(run_func=False)
                else:
                    self.button_right.press(run_func=False)
            else:
                self.button_left.unpress()
                self.button_center.unpress()
                self.button_right.unpress()

            if len(set(config_dic['foreground'])) == 1:
                self.button_color.set(config_dic['foreground'][0])
            else:
                self.button_color.set()

            if len(set(config_dic['background'])) == 1:
                self.button_highlight.set(config_dic['background'][0])
            else:
                self.button_highlight.set()

            if len(set(config_dic['spacing2'])) == 1:
                self.button_lines.set(config_dic['spacing2'][0])
            else:
                self.button_lines.set('')
            if len(set(config_dic['spacing1'])) == 1:
                self.button_paragraphs.set(config_dic['spacing1'][0])
            else:
                self.button_paragraphs.set('')

            try:
            # If the text is indented, pressing <Return> would call the binding crashing the app.
                self.text.unbind('<Return>', self.counting_bullets)
            except:
                pass
            try:
                self.text.unbind('<Return>', self.counting_numbers)
            except:
                pass
            
        else:
            tag = self.text.tag_names('insert-1c')
            if tag == ():
                tag = self.text.tag_names('insert')
                if tag == ():
                # In rare cases (I have found one, when the whole text gets deleted), all
                # the tags are gone so we need to create a default one by redirecting to writing().
                    self.writing()
                    tag = self.text.tag_names('insert-1c')
                self.set_widgets(tag)
            else:
                self.set_widgets(tag)


    def set_widgets(self, tag):

        '''Sets the styling widgets to the values of the given tag.'''

        for tagconfig in self.all_tag_configs:
            config = self.text.tag_cget(tag, tagconfig)
            if tagconfig == 'font':
                font = font_unpack(config)
                self.button_font.set(font[0])
                self.button_size.set(font[1])
                if 'bold' in font:
                    self.button_bold.press(set_state='pressed')
                else:
                    self.button_bold.press(set_state='!pressed')
                if 'italic' in font:
                    self.button_italic.press(set_state='pressed')
                else:
                    self.button_italic.press(set_state='!pressed')
            elif tagconfig == 'underline':
                if config == '1':
                    self.button_underline.press(set_state='pressed')
                else:
                    self.button_underline.press(set_state='!pressed')
            elif tagconfig == 'justify':
                if config == 'left':
                    self.button_left.press(run_func=False)
                elif config == 'center':
                    self.button_center.press(run_func=False)
                elif config == 'right':
                    self.button_right.press(run_func=False)
            elif tagconfig == 'foreground':
                self.button_color.set(config)
            elif tagconfig == 'background':
                self.button_highlight.set(config)
            elif tagconfig == 'spacing2':
                self.button_lines.set(config)
            elif tagconfig == 'spacing1':
                self.button_paragraphs.set(config)
            elif tagconfig == 'lmargin1':                               
                if config != '0':
                    if self.text.get(self.text.index('insert') + ' linestart+1c') in [num for num in '123456789']:
                        self.counting_numbers = self.text.bind('<Return>', lambda e: self.numbering('some_arg'))
                    else:
                        self.counting_bullets = self.text.bind('<Return>', self.bulleting)
                else:
                    try:
                        self.text.unbind('<Return>', self.counting_bullets)
                    except:
                        pass
                    try:
                        self.text.unbind('<Return>', self.counting_numbers)
                    except:
                        pass
                    

    def selection_in_progress(self, *args):

        '''A functions that helps determine if other functions should behave
        as if a part of the text was selected or not.'''

        if ('sel' in self.text.tag_names('insert-1c') or 
            'sel' in self.text.tag_names('insert')):
            self.text.tag_raise('sel')
            # The sel tag needs to be raised, otherwise highlighted color
            # will dominate the selection color. 
            return True
        else:
            return False


    def dump_selection(self, *args):

        ''' Creating my own dump() function that applies only for the selected
         part of text as Tk.dump function does not do that. Plus, I make it
         into a nice dictionary that indicates the beginning and end index
         of each tag in the selection range. '''

        selection = self.text.tag_ranges('sel')
        dump_dic = {}
        last_tag = ''
        for n, ch in enumerate(self.text.get(selection[0], selection[1])):
            ch_indx = str(selection[0]) + f'+{n}c'
            try:
            # When the whole text is selected, sometimes there is a character (probably \n)
            # with just the 'sel' tag, thus the index[1] cannot be found, thus the try/except loop. 
                for i in self.text.tag_names(ch_indx):
                    if i.startswith('tag'):
                        tag = i
            except:
                break

            if tag != last_tag:
                if last_tag == '':
                    dump_dic[tag] = {'begin': [ch_indx], 'end': []}
                    last_tag = tag
                elif tag not in dump_dic.keys():
                    dump_dic[last_tag]['end'].append(ch_indx)
                    dump_dic[tag] = {'begin': [ch_indx], 'end': []}
                    last_tag = tag
                elif tag in dump_dic.keys():
                    dump_dic[last_tag]['end'].append(ch_indx)
                    dump_dic[tag]['begin'].append(ch_indx)
                    last_tag = tag

        for tag in dump_dic.keys():
            if len(dump_dic[tag]['begin']) > len(dump_dic[tag]['end']):
                dump_dic[tag]['end'].append(str(selection[1]))

        return dump_dic


    def config_selection(self, *args):

        '''Configures the selected text with the requested configurations.'''

        tag_dic = self.dump_selection()
        for tag in tag_dic.keys():
            newtag = next(self.yield_tag())
            for indice, beginning in enumerate(tag_dic[tag]['begin']):
                self.text.tag_remove(tag, tag_dic[tag]['begin'][indice], 
                    tag_dic[tag]['end'][indice])
                self.text.tag_add(newtag, tag_dic[tag]['begin'][indice], 
                    tag_dic[tag]['end'][indice])
                # We change the old tag to a new one in all its ranges.

            if 'font' in args:

                font = font_unpack(self.text.tag_cget(tag, 'font'))

                if 'bold' in args:
                    if self.button_bold.pressed:
                        font.remove('bold')
                    elif 'bold' not in font:
                        font.append('bold')
                    self.text.tag_configure(newtag, font=font)

                elif 'italic' in args:
                    if self.button_italic.pressed:
                        font.remove('italic')
                    elif 'italic' not in font:
                        font.append('italic')
                    self.text.tag_configure(newtag, font=font)

                elif args[1] in self.button_font.values:
                    for i in font:
                        if i in self.button_font.values:
                            font.remove(i)
                    font.insert(0, args[1])
                    self.text.tag_configure(newtag, font=font)

                elif args[1] in [str(n) for n in range(100)]:
                # For combobox_size.
                    for i in font:
                        if i in [str(n) for n in range(100)]:
                            font.remove(i)
                    font.insert(1, args[1])
                    self.text.tag_configure(newtag, font=font)

            elif 'underline' in args:

                if self.button_underline.pressed:
                    self.text.tag_configure(newtag, underline=0)
                else:
                    self.text.tag_configure(newtag, underline=1)

            elif 'foreground' in args:

                self.text.tag_configure(newtag, foreground=args[1])

            elif 'background' in args:

                self.text.tag_configure(newtag, background=args[1])

            elif 'justify' in args:

                if 'left' in args:
                    self.text.tag_configure(newtag, justify='left')
                elif 'center' in args:
                    self.text.tag_configure(newtag, justify='center')
                elif 'right' in args:
                    self.text.tag_configure(newtag, justify='right')

            elif 'spacing2' in args:

                self.text.tag_configure(newtag, spacing2=args[1])

            elif 'spacing1' in args or 'spacing3' in args:

                self.text.tag_configure(newtag, **{args[0]: args[1]})

            elif 'lmargin1' in args or 'lmargin2' in args:

                if 'adjust' in args:
                    self.text.tag_configure(newtag, **{args[0]: args[2]})
                else:
                    lmargin = self.text.tag_cget(tag, args[0])
                    if lmargin == "" or lmargin == '0':
                        lmargin = '10m'
                    else:
                        lmargin = int(lmargin[:2])
                        lmargin += 10
                        lmargin = str(lmargin) + 'm'
                    self.text.tag_configure(newtag, **{args[0]: lmargin})

            almost_all_configs = self.all_tag_configs.copy()
            almost_all_configs.remove(args[0])
            for tagconfig in almost_all_configs:
                config = self.text.tag_cget(tag, tagconfig)
                self.text.tag_configure(newtag, **{tagconfig: config})

            self.save()

########################### FUNCTIONS THAT WORK WITH CONFIG_SELECTION

############## TEXT STYLING
    def b_i_u(self, arg2=None, underline=False):

        ''' Make text Bold/Italic/Underlined.'''

        if underline:
            arg1 = 'underline'
        else:
            arg1 = 'font'

        if self.selection_in_progress():
            self.config_selection(arg1, arg2)
        else:
            self.changed_configs = True


    def font(self, *args):

        if self.selection_in_progress():
            self.config_selection('font', self.button_font.get())
        else:
            self.changed_configs = True


    def size(self, *args):

        if self.selection_in_progress():
            self.config_selection('font', self.button_size.get())
        else:
            self.changed_configs = True


    def color(self, color): 

        if self.selection_in_progress():
            self.config_selection('foreground', color)
        else:
            self.changed_configs = True


    def highlight(self, color):

        if not color:
            color = ''
        if self.selection_in_progress():
            self.config_selection('background', color)
        else:
            self.changed_configs = True

############### PARAGRAPH STYLING
    def justify(self, *args):

        if self.selection_in_progress():
        # Pushes the selected paragraph.
            sel_first = self.text.index('sel.first')
            sel_last = self.text.index('sel.last')
            start = sel_first +' linestart -1c'
            end = sel_last +' lineend +1c'
            self.text.tag_remove('sel', 1.0, 'end')
            self.text.tag_add('sel', start, end)
            self.config_selection('justify', args[0])
            # If no index is specified, the args tuple would be passed in.
            self.text.tag_remove('sel', 1.0, 'end')
            self.text.tag_add('sel', sel_first, sel_last)
        else:
        # If there is no selection, the paragraph holding the "insert" cursor will be pushed.
            start = self.text.index('insert') +' linestart -1c'
            end = self.text.index('insert') +' lineend +1c'
            self.text.tag_add('sel', start, end)
            self.config_selection('justify', args[0])
            self.text.tag_remove('sel', 1.0, 'end')


    def spacing(self, *args):

        '''Deals with line spacing and paragraph spacing.'''

        if 'lines' in args:
            selection = self.button_lines.get()
        elif 'paragraphs' in args:
            selection = self.button_paragraphs.get()

        if self.selection_in_progress():
        # Changes spacing of the selected paragraph.
            sel_first = self.text.index('sel.first')
            sel_last = self.text.index('sel.last')
            start = sel_first +' linestart -1c'
            end = sel_last +' lineend +1c'
            self.text.tag_remove('sel', 1.0, 'end')
            self.text.tag_add('sel', start, end)
            if 'lines' in args:
                self.config_selection('spacing2', selection)
            elif 'paragraphs' in args:
                self.config_selection('spacing1', selection)
                self.config_selection('spacing3', selection)
            self.text.tag_remove('sel', 1.0, 'end')
            self.text.tag_add('sel', sel_first, sel_last)
        else:
        # If there is no selection, the desired spacing will be added to the 
        # whole text.
            self.text.tag_add('sel', 1.0, 'end')
            if 'lines' in args:
                self.config_selection('spacing2', selection)
            elif 'paragraphs' in args:
                self.config_selection('spacing1', selection)
                self.config_selection('spacing3', selection)
            self.text.tag_remove('sel', 1.0, 'end')


    def indent(self, *args):  

        ''' This function deals with both cases of indented text: bullet list and number list.'''

        if self.selection_in_progress():
            sel_first = self.text.index('sel.first')
            sel_last = self.text.index('sel.last')
            start = sel_first +' linestart'
            end = sel_last +' lineend'
            self.text.tag_remove('sel', 1.0, 'end')

            first_line = int(sel_first.split('.')[0])
            last_line = int(sel_last.split('.')[0])

            if int(self.text.index('insert').split('.')[0]) == first_line:
            # By the end of the function, after all the new characters are inserted, in Tk the selection
            # field moves accordingly, however, the "insert" cursor remains on the original character.
            # Thus, the "insert" cursor needs to be adjusted.
                insert_cursor = lambda: self.text.mark_set('insert', sel_first)
            else:
                insert_cursor = lambda: self.text.mark_set('insert', sel_last) 

            if self.text.tag_cget(self.text.tag_names(start), 'lmargin1') != '0':
            # If the text is already indented, we always remove any existing bulleting or numbering.
                the_char = self.text.get(start +'+1c')
                all_chars = [n for n in '0123456789•.\t ']
                some_chars = [n for n in '•.\t ']
                for line in range(first_line, last_line + 1): 
                    idx = self.text.index(str(line) + '.0')
                    used_chars = []
                    for ch in self.text.get(idx, idx +' lineend'):
                        if ch in all_chars and ch not in used_chars:
                            self.text.delete(idx)
                            if ch in some_chars:
                                used_chars.append(ch)
                        else:
                            break
                self.text.tag_add('sel', start, end)
                self.config_selection('lmargin1', 'adjust', '0')
                self.config_selection('lmargin2', 'adjust', '0')
                self.text.tag_remove('sel', 1.0, 'end')
                if (the_char == '•' and 'bullet' in args) or (the_char in [num for num in '0123456789'] and 'number' in args):
                    args = None
                # If the type of listing matches the call (list was bulleted and the call was via bulleting button),
                # we remove the argument not allowing the second part of the function.
            
            if args:
            # This part of the function will make the reqested type of list.
                self.text.tag_add('sel', start, end)
                self.config_selection('lmargin1')
                self.config_selection('lmargin2')
                self.text.tag_remove('sel', 1.0, 'end')

                for line in range(first_line, last_line + 1): 
                    idx = self.text.index(str(line) + '.0')
                    if 'bullet' in args:
                        for i in [' ', u"\u2022", '\t']:
                            self.text.insert(idx, i)
                            self.writing()
                            self.text.tag_remove(self.text.tag_names(idx), idx)
                            self.text.tag_add(self.text.tag_names(idx + '+1c'), idx)
                            idx += '+1c'
                    elif 'number' in args:
                        for i in [' ', '1', '.', '\t']:
                            self.text.insert(idx, i)
                            self.writing()
                            self.text.tag_remove(self.text.tag_names(idx), idx)
                            self.text.tag_add(self.text.tag_names(idx + '+1c'), idx)
                            idx += '+1c'
                if 'number' in args:
                    self.check_numbers(sel_last)

            root.update()
            # Need to flush all delete and insert events coming from the text editing engine functions, 
            # otherwise they will run after the func and catch the presence of 'sel' tag.
            self.text.tag_add('sel', sel_first, sel_last)
            insert_cursor()

        else:
        # If no selection, then we start a bulletlist/numbering in a new line.
            if 'bullet' in args:
                self.bulleting()
                self.text.tag_add('sel', 'insert-3c', 'insert')
            elif 'number' in args:
                self.numbering()
                self.text.tag_add('sel', 'insert-4c', 'insert')
            self.config_selection('lmargin1')
            self.config_selection('lmargin2')
            self.text.tag_remove('sel', 1.0, 'end')
            if 'number' in args:
                self.check_numbers()
        return "break"


    def bulleting(self, *args):

        '''Function that inserts the bullet.'''

        self.text.insert('insert', '\n')
        self.writing()
        for i in [' ', u"\u2022", '\t']:
            self.text.insert('insert', i)
            self.writing()
        return "break"


    def numbering(self, *args):

        ''' Function that inserts the number.'''

        self.text.insert('insert', '\n')
        self.writing()
        for i in [' ', '1', '.', '\t']:
            self.text.insert('insert', i)
            self.writing()
        if args:
        # When passed on from indent(), the indentation needs to be done before running check_numbers, otherwise
        # the numbering will mess up, thus this is only done if numbering() is called from the <Return> binding.
            self.check_numbers()
        return "break"


    def check_numbers(self, *args):
    
        '''This beauty gets the number list ordered.'''

        if args: 
        # If passed from the indent() selection func, we take the last selected line.                     
            current_line = args[0]
        else:
        # If passed from other funcs, we take the place of an insert cursor.                        
            current_line = self.text.index('insert-1c')

        next_line = str(int(current_line.split('.')[0]) +1) + '.0'
        while self.text.tag_names(next_line) != () and self.text.tag_cget(self.text.tag_names(next_line), 'lmargin1') == self.text.tag_cget(self.text.tag_names(current_line), 'lmargin1') and self.text.get(next_line +' linestart+1c') in [num for num in '0123456789']:
            current_line = next_line
            next_line = str(int(current_line.split('.')[0]) +1) + '.0'
        bottom_line = int(current_line.split('.')[0])
        # We go to the very bottom of numbered lines. Relevant if the number is inserted
        # in between other numbers.

        before_line = str(int(current_line.split('.')[0]) -1) + '.0'
        line_count = 1
        while self.text.tag_cget(self.text.tag_names(before_line), 'lmargin1') == self.text.tag_cget(self.text.tag_names(current_line), 'lmargin1') and before_line != '0.0' and self.text.get(before_line +' linestart+1c') in [num for num in '0123456789']:
            line_count += 1
            current_line = before_line
            before_line = str(int(current_line.split('.')[0]) -1) + '.0'
        # We go to the very top while counting how many lines are being numbered.

        top_line = int(current_line.split('.')[0])
        top_numb = ''
        for ch in self.text.get(current_line +'+1c', current_line +' lineend'):
            if ch in [num for num in '0123456789']:
                top_numb += ch
            else:
                break
        if top_numb == '':
            top_numb = '1'

        numb = iter(range(int(top_numb), line_count +int(top_numb)))
        for line in range(top_line, bottom_line +1):
        # We now go from the top to the bottom again line by line and change
        # any numbers that needs changing.
            idx = str(line) + '.1'
            old_numb = ''
            new_numb = str(next(numb))
            for n, ch in enumerate(self.text.get(idx, idx +' lineend')):
                if ch in [num for num in '0123456789']:
                    old_numb += ch
                else:
                    break
            del_idx = self.text.index(idx + f'+{n}c')
            if old_numb == new_numb:
            # If the line is numbered correctly, we continue.
                continue
            else:
                self.text.delete(idx, del_idx)
                for n in new_numb:
                    self.text.insert(idx, n)
                    self.writing()
                    idx += '+1c'


################################# COPY/CUT/PASTE

    def check_clipboard(self, text):

        ''' After the first "local" copying, this machine will start running and will not stop
            until some text from outside of the application gets copied. This is needed for the
            paste() funcion to know how to appropriatelly set up its tags as there is no formatting 
            when copying from outside of the application.'''  

        check_for_new = pyperclip.paste()
        if text == check_for_new:
            root.after(100, self.check_clipboard, text)
            self.local_clipboard = True
        else:
            self.local_clipboard = False


    def copy(self, *args):

        if self.selection_in_progress():
            text = self.text.get('sel.first', 'sel.last')
            pyperclip.copy(text)
            self.clipboard_tags = self.dump_selection_clipboard()
            self.local_clipboard = True
            self.check_clipboard(text)
            
        return "break"


    def cut(self, *args):

        self.copy()
        if self.selection_in_progress():
            self.text.delete('sel.first', 'sel.last')

        return 'break'
        # We need to stop the default Tk binding for Control-c as it starts
        # interfering with the tk.call function in the ImprovedText.


    def paste(self, *args):

        text = pyperclip.paste()
        text = "".join([c for c in text if ord(c) <= 65535])
        # Removing all characters > 65535 (that's the range for Tkinter).

        begin_idx = self.text.index('insert')
        self.text.insert('insert', text)

        if self.local_clipboard:
        # Retaining the text configurations if the text was copied locally.
            for n, ch in enumerate(self.text.get(begin_idx, 'insert')):
                current_idx = begin_idx + f'+{n}c'
                self.text.tag_remove(self.text.tag_names(current_idx), current_idx)

            tags = self.clipboard_tags['sequence'][::2]
            span = iter(self.clipboard_tags['sequence'][1::2])
            used_tags = {}

            current_idx = begin_idx
            for tag in tags:
                if tag not in used_tags.keys():
                    newtag = next(self.yield_tag())
                    used_tags[tag] = newtag
                    current_span = str(next(span))
                    self.text.tag_add(newtag, current_idx, current_idx + f'+{current_span}c')
                    current_idx = current_idx + f'+{current_span}c'
                    for config in self.all_tag_configs:
                        self.text.tag_configure(newtag, **{config: self.clipboard_tags[tag][config]})
                    self.save()
                else:
                    current_span = str(next(span))
                    self.text.tag_add(used_tags[tag],current_idx, current_idx + f'+{current_span}c')
                    current_idx = current_idx + f'+{current_span}c'

            end_idx = self.text.index('insert')
            self.text.mark_set('insert', begin_idx)
            self.deletion()
            self.text.mark_set('insert', end_idx)
            # This will adjust the first pasted line to match the configs of the line its being merged to.
        else:
            # If the text is copied from outside the app, we apply the tag which is on the left to the inserted text.
            # Otherwise, the inserted text will have no tags and mess everything up.
            tag = ''
            for i in self.text.tag_names(begin_idx + ' -1c'):
                if i.startswith('tag'):
                    tag = i
            if tag == '':
                tag = self.text.tag_names('insert')
            # If the text is empty.
            self.text.tag_add(tag, begin_idx, 'insert')

        return 'break'
                

    def delete(self, *args):

        if self.selection_in_progress():
            self.text.delete('sel.first', 'sel.last')
        self.deletion()


    def dump_selection_clipboard(self, *args):

        '''Creates a sequence of every tag and their spans in the selection 
        range, as well as exports each tag's configurations.'''

        dump_dic = {'sequence': []}
        last_tag = ''
        span = 1
        for n, ch in enumerate(self.text.get('sel.first', 'sel.last')):
            ch_indx = 'sel.first' + f'+{n}c' 
            for i in self.text.tag_names(ch_indx):
                if i.startswith('tag'):
                    tag = i
            if tag == last_tag:
                span += 1
            else:
                if last_tag == '':
                    dump_dic['sequence'].append(tag)
                    last_tag = tag
                    dump_dic[tag] = {}
                    for config in self.all_tag_configs:
                        dump_dic[tag][config] = self.text.tag_cget(tag, config)
                elif tag not in dump_dic['sequence']:
                    dump_dic['sequence'].append(span)
                    dump_dic['sequence'].append(tag)
                    span = 1
                    last_tag = tag
                    dump_dic[tag] = {}
                    for config in self.all_tag_configs:
                        dump_dic[tag][config] = self.text.tag_cget(tag, config)
                elif tag in dump_dic['sequence']:
                    dump_dic['sequence'].append(span)
                    dump_dic['sequence'].append(tag)
                    span = 1
                    last_tag = tag
        dump_dic['sequence'].append(span)

        return dump_dic
                

################################### SAVING AND STARTUP

    def yield_tag(self):

        subject_id = self.subject_menu.get()
        lecture_id = self.lecture_menu.get()
        for i in count(1):
            tag_id = 'tag_#' + str(i)
            if tag_id not in subject_dic[subject_id]['lecture_dic'][lecture_id]:
                yield tag_id


    def populate_tags(self, *args): 

        '''Populates the text field with the tags that come along the loaded text.'''

        subject_id = self.subject_menu.get()
        lecture_id = self.lecture_menu.get()
        lecture = subject_dic[subject_id]['lecture_dic'][lecture_id]
        for i in lecture:
            if i.startswith('tag_#'):
                tag = i
                for n, start_idx in enumerate(lecture[tag]['begin']):
                    end_idx = lecture[tag]['end'][n]
                    self.text.tag_add(tag, start_idx, end_idx)
                for tagconfig in self.all_tag_configs:
                    value = lecture[tag][tagconfig]
                    self.text.tag_configure(tag, **{tagconfig: value})


    def dump_save(self, *args):

        '''Building on Tk's dump() function to save tags and all of the information
        about them.'''

        global subject_dic
        subject_id = self.subject_menu.get()
        lecture_id = self.lecture_menu.get()

        lecture = subject_dic[subject_id]['lecture_dic'][lecture_id]

        fake_dic = lecture.copy()
        for i in fake_dic.keys():
            if i.startswith('tag_#'):
                tag = i
                del lecture[tag]

        dumpster = self.text.dump(1.0, 'end', tag=True)
        for i in dumpster:
            tag = i[1]
            indx = i[2]
            if i[0] == 'tagon' and tag != 'sel':
                if tag not in lecture.keys():
                    lecture[tag] = {'begin': [indx]}
                    lecture[tag]['end'] = []
                else:
                    lecture[tag]['begin'].append(indx)
            elif i[0] == 'tagoff' and tag != 'sel':
                lecture[tag]['end'].append(indx)

        for i in lecture.keys():
            if i.startswith('tag_#'):
                tag = i
                if len(lecture[tag]['begin']) > len(lecture[tag]['end']):
                    lecture[tag]['end'].append('end')
                    # Can this possibly need to be 'end-1c'?
                for tagconfig in self.all_tag_configs:
                    value = self.text.tag_cget(tag, tagconfig)
                    lecture[tag][tagconfig] = value


    def save(self):

        ''' Writes to the data.py file and saves dictionaries for the undo/redo stack.'''

        try:
            global currently_editing

            subject = self.subject_menu.get()
            lecture = self.lecture_menu.get()

            text = self.text.get(1.0, 'end').rstrip()
            subject_dic[subject]['lecture_dic'][lecture]['text'] = text
            self.dump_save()
            subject_dic[subject]['lecture_dic'][lecture]['last_modified'] = str(datetime.now().replace(microsecond=0))
            subject_dic[subject]['current_lecture'] = lecture
            subject_dic[subject]['lecture_order'] = self.lecture_menu.get_order()


            currently_editing = (subject, lecture)

            with open('data.py', 'w', encoding="utf-8") as data:
                data.write(f'''subject_dic = {subject_dic}\ncurrently_editing = {currently_editing}\nsorting_info = {sorting_info}''')
            
            ### Undoredo stack.
            do_undoredo = False
            count_tags = len([i for i in subject_dic[subject]['lecture_dic'][lecture] if i.startswith('tag')])
            count_undoredo_tags = len([i for i in self.undoredo[self.undoredo_idx] if i.startswith('tag')])
            if count_tags != count_undoredo_tags:
                do_undoredo = True
            # After much of trial and error, measuring the number of tags turned out to be the working solution as when there are newline
            # characters, the application applies tags automatically to bigger ranges of text (writing func) which is essential to do, but it
            # messes up the undoredo stach as the subject dictionaries start to not match and undoredo stack gets stuck.

            if ''.join(subject_dic[subject]['lecture_dic'][lecture]['text']).split() != ''.join(self.undoredo[self.undoredo_idx]['text']).split():
                do_undoredo = True
            # It is also important to remove all the whitespaces when comparing the text also because of the newline characters, because Tk() applies
            # or remembers newlines automatically and it is hard to say when and how much it does that.

            if do_undoredo:
            # If there were changes made to the text, save it in the self.undoredo list of dictionaries and remove
            # any dictionaries that go after the current one so that a new line of "redoes" can be made.
                self.undoredo_idx += 1
                self.undoredo = self.undoredo[:self.undoredo_idx]
                self.undoredo.append(copy.deepcopy(subject_dic[subject]['lecture_dic'][lecture]))


        except: 
            '''Catching an exception of no previous book to save (program is
            being opened for the first time).'''
            pass


    def populate_subjects(self):

        '''Load the subjects and add them to the Subject Menu.'''

        if subject_dic != {}:
            for subject_id in subject_dic:
                self.subject_menu.insert(subject_id, press=False)

            self.subject_menu.set(currently_editing[0])

            self.text.focus()
            self.text.see('insert') 
            
        else:
            self.button_font.set('Calibri')
            self.button_size.set('12')
            self.button_lines.set('0')
            self.button_paragraphs.set('0')
            self.button_color.set('#000000')
            self.add_subject(first_time=True)


##################################### SUBJECT AND LECTURE MENU INTERACTIONS WITH THE APPLICATION

    def change_subject(self, *args):

        '''User presses on another subject.'''

        subject_id = self.subject_menu.get()

        for button in self.lecture_menu.menu.grid_slaves():
            button.grid_forget()

        for lecture_id in subject_dic[subject_id]['lecture_order']:
            self.lecture_menu.insert(lecture_id, press=False)

        current_lecture = subject_dic[subject_id]['current_lecture']
        self.lecture_menu.set(current_lecture)


    def change_lecture(self, resetundoredo=True):

        '''User presses on another lecture.'''

        self.text.delete(1.0, 'end')
        subject_id = self.subject_menu.get()
        lecture_id = self.lecture_menu.get()
        self.text.insert(1.0, subject_dic[subject_id]['lecture_dic'][lecture_id]['text'])
        self.text.edit_reset()
        # This command seems to be neccessary when loading/adding
        # a book, otherwise, something goes wrong in the Tkinter,
        # most of the tags get deleted and an exception appears as the
        # cursor() command cannot find the tags anymore.

        self.populate_tags()
        self.text.focus()

        if resetundoredo:
        # If the user changes lecture, undo/redo stack resets as brand new text enters into the window.
            self.resetundoredo()


    def add_subject(self, first_time=False):

        '''First part of adding a new subject.'''

        self.save()

        self.text.tag_remove('sel', 1.0, 'end')
        # If the new item is added while the selection is being done, some Frame one
        # widgets could be blank which will affect this function badly.

        self.text.grid_remove()
        self.fake_text = ImprovedText(self.main_three, relief='flat', wrap='word', tabs='17m',
            font=['Calibri', 12], selectbackground='SystemHighlight', inactiveselectbackground='SystemHighlight', height=30)
        self.fake_text.grid(column=0, row=0, sticky='NSEW')
        # Hiding the text field with an empty text field.

        self.lecture_menu.pack_forget()
        self.fake_lectures = LectureMenu(self.main_two, caller=self)
        self.fake_lectures.pack(side='left', expand=True, fill='x')
        # Hiding the lecture menu with an empty lecture menu.

        entry = self.subject_menu.create_entry('add_new', first_time)

        entry.bind('<Return>', lambda e: entry.validate('add_new', self.new_subject))
        root.bind('<1>', lambda e: self.click_add_new(menu=self.subject_menu, first_time=first_time))
        self.subject_menu.win.bind('<1>', lambda e: self.click_add_new(menu=self.subject_menu, first_time=first_time))
        if not first_time:
            entry.bind('<Escape>', lambda e: self.undo_new(menu=self.subject_menu))

        return 'break'


    def new_subject(self, contents):

        '''If the user does not cancel adding a new subject, this function adds it and saves it.'''

        self.unbind_root()

        subject_id = next(self.yield_id())
        lecture_id = '@1'
        date_created = str(datetime.now().replace(microsecond=0))
        subject_dic[subject_id] = {'name': contents, 'date_created': date_created, 'current_lecture': lecture_id, 'lecture_order': [lecture_id],'lecture_dic': {lecture_id: {'name': 'Lecture 1', 'date_created': date_created, 'last_modified': date_created, 'text': ''}}}    
        currently_editing = (subject_id, lecture_id)
        with open('data.py', 'w', encoding="utf-8") as data:
            data.write(f'''subject_dic = {subject_dic}\ncurrently_editing = {currently_editing}\nsorting_info = {sorting_info}''')

        self.fake_text.grid_forget()
        self.text.grid()
        self.fake_lectures.pack_forget()
        self.lecture_menu.pack(side='left', expand=True, fill='x')

        font = self.button_font.get()
        size = self.button_size.get()
        lines = self.button_lines.get()
        paras = self.button_paragraphs.get()
        fore = self.button_color.get()
        back = self.button_highlight.get()

        self.subject_menu.insert(subject_id)

        for tag in self.text.tag_names(1.0):
            self.text.tag_delete(tag)
        # Replacing automatically appeared tags (thanks to functions like 'writing')
        # with a brand new tag that transfers some of the configurations from the user's previously edited text.
        newtag = next(self.yield_tag())
        self.text.tag_add(newtag, 1.0, 'end')
        self.text.tag_configure(newtag, font=[font, size], underline=0, foreground=fore, background=back, justify='left', spacing2=lines, spacing1=paras, spacing3=paras, lmargin1=0, lmargin2=0)
        self.cursor()
        # Set the widgets to the tag's configurations.
        self.save()
        self.resetundoredo()
        # Need to resetundoredo after applying these initial tags, otherwise if a user will undo to the very beginning, 
        # the subject dictionaries will not match and undoredo stack will get stuck and lost.

        self.lecture_menu.mouse_button = self.lecture_menu.get('@1')
        self.rename(menu=self.lecture_menu)


    def add_lecture(self, *args):

        '''First part of adding a new lecture.'''

        if self.lecture_menu.menu.grid_size()[0] < 15:
        # I am limiting the number of possible lectures because the more lectures, the more unstable 
        # the lecture_menu engine that controls the tab funcionality seems to get.
            self.save()

            self.text.tag_remove('sel', 1.0, 'end')
            # If the new item is added while the selection is being done, some Frame one
            # widgets could be blank which will affect this function badly.

            self.text.grid_remove()
            self.fake_text = ImprovedText(self.main_three, relief='flat', wrap='word', tabs='17m',
                font=['Calibri', 12], selectbackground='SystemHighlight', inactiveselectbackground='SystemHighlight', height=30)
            self.fake_text.grid(column=0, row=0, sticky='NSEW')

            entry = self.lecture_menu.create_entry('add_new')

            entry.bind('<Return>', lambda e: entry.validate('add_new', self.new_lecture))
            entry.bind('<Escape>', lambda e: self.undo_new(menu=self.lecture_menu))
            root.bind('<1>', lambda e: self.click_add_new(menu=self.lecture_menu))

            return 'break'
        else:
            messagebox.showerror(message='You have reached the maximum number of lectures for one subject.', 
                detail='To create more lectures, please start a new subject.', title='Maximum Lectures Reached', parent=self.subject_menu.win)


    def new_lecture(self, contents, fake_text=True):

        '''If the user does not cancel adding a new lecture, this function adds it and saves it.'''

        self.unbind_root()

        subject_id = self.subject_menu.get()
        lecture_id = next(self.yield_id(lecture=True))
        date_created = str(datetime.now().replace(microsecond=0))
        subject_dic[subject_id]['lecture_dic'][lecture_id] = {'name': contents, 'date_created': date_created, 'last_modified': date_created, 'text': ''}
        currently_editing = (subject_id, lecture_id)
        with open('data.py', 'w', encoding="utf-8") as data:
            data.write(f'''subject_dic = {subject_dic}\ncurrently_editing = {currently_editing}\nsorting_info = {sorting_info}''')

        if fake_text:
            self.fake_text.grid_forget()

        self.text.grid()
        font = self.button_font.get()
        size = self.button_size.get()
        lines = self.button_lines.get()
        paras = self.button_paragraphs.get()
        fore = self.button_color.get()
        back = self.button_highlight.get()

        self.lecture_menu.insert(lecture_id)

        for tag in self.text.tag_names(1.0):
            self.text.tag_delete(tag)
        # Replacing automatically appeared tags (thanks to functions like 'writing')
        # with a brand new tag that transfers some of the configurations from the user's previously edited text.
        newtag = next(self.yield_tag())
        self.text.tag_add(newtag, 1.0, 'end')
        self.text.tag_configure(newtag, font=[font, size], underline=0, foreground=fore, background=back, justify='left', spacing2=lines, spacing1=paras, spacing3=paras, lmargin1=0, lmargin2=0)
        self.cursor()
        # Set the widgets to the tag's configurations.
        self.text.focus()

        self.save()
        self.resetundoredo()
        # Need to resetundoredo after applying these initial tags, otherwise if a user will undo to the very beginning, 
        # the subject dictionaries will not match and undoredo stack will get stuck and lost.


    def undo_new(self, menu):

        '''Cancel adding a new subject or lecture.'''

        self.unbind_root()

        menu.entry.grid_forget()

        if menu == self.subject_menu:
            menu.set(currently_editing[0])
            self.fake_lectures.pack_forget()
            self.lecture_menu.pack(side='left', expand=True, fill='x')
        else:
            menu.set(currently_editing[1])

        self.fake_text.grid_forget()
        self.text.grid()


    def click_add_new(self, menu, first_time=False):

        '''Clicking anywhere outside the entry field that appears when adding a new
        sub/lec will act the same as a <Return> binding.'''

        entry = menu.entry
        mouse_location = menu.get_mouse(remember_button=False)

        if str(mouse_location) != str(entry):
            self.unbind_root()
            if not first_time:
                if menu == self.subject_menu:
                    add_func = self.new_subject
                else:
                    add_func = self.new_lecture
                entry.validate('add_new', add_func, self.undo_new)          
            else:
                # This applies only to the subject_menu.
                entry.validate('add_new', self.new_subject)
            

    def unbind_root(self, motion=True):

        try:
            root.unbind('<1>')
            root.unbind('<3>')
            self.subject_menu.win.unbind('<1>')
        except:
            pass

        
    def yield_id(self, lecture=False):

        if lecture:
            subject_id = self.subject_menu.get()
            for i in count(1):
                lecture_id = '@' + str(i)
                if lecture_id not in subject_dic[subject_id]['lecture_dic']:
                    yield lecture_id
        else:
            for i in count(1):
                subject_id = '#' + str(i)
                if subject_id not in subject_dic:
                    yield subject_id


    def post_subject(self, event):

        '''Posts the subject context menu.'''

        self.subject_menu.get_mouse()
        self.context_subject.post(event.x_root, event.y_root)


    def post_lecture(self, event):

        '''Posts the lecture context menu.'''

        mouse_location = root.winfo_containing(root.winfo_pointerx(), root.winfo_pointery())
        if 'lecturemenu.!frame' in str(mouse_location):
            self.lecture_menu.get_mouse()
            self.context_lecture.post(event.x_root, event.y_root)


    def rename(self, menu):
        
        '''Rename the desired subject or lecture.'''

        entry = menu.create_entry('rename')

        entry.bind('<Return>', lambda e: entry.validate('rename', self.new_name))
        entry.bind('<Escape>', lambda e: self.undo_rename(menu))
        root.bind('<1>', lambda e: self.click_rename(menu), '+')

        if menu == self.subject_menu:
        # The root bindings do not register on the subject_menu window for some reason.
            self.subject_menu.win.bind('<1>', lambda e: self.click_rename(menu), '+')


    def new_name(self, contents, menu):

        '''If the new name gets validated, it will be accepted and saved here.'''

        global subject_dic

        self.unbind_root()

        item = menu.mouse_button
        item.configure(text=contents)

        if menu == self.subject_menu:
            subject_dic[item.id]['name'] = contents
            item.shorten_name()
        else:
            subject_id = self.subject_menu.get()
            subject_dic[subject_id]['lecture_dic'][item.id]['name'] = contents
            item.tab_label.configure(text=contents)
            self.lecture_menu.adjust_width()

        self.text.focus()

        self.save()


    def click_rename(self, menu):

        '''Clicking anywhere outside the entry field that appears when renaming
        sub/lec will act the same as a <Return> binding.'''

        entry = menu.entry
        mouse_location = menu.get_mouse(remember_button=False)

        if str(mouse_location) != str(entry):
            self.unbind_root()
            entry.validate('rename', self.new_name, self.undo_rename)


    def undo_rename(self, menu):

        '''User cancels the renaming.'''

        self.unbind_root()

        menu.entry.grid_forget()
        menu.mouse_button.grid()

        self.text.focus()


############################ OTHER FUNCTIONS

    def load(self, direction):

        '''Do the undo/redo if there is anything to undo or redo.'''

        global subject_dic

        if direction == 'undo':
            needed_txt = self.undoredo_idx -1
        elif direction == 'redo':
            needed_txt = self.undoredo_idx +1

        if needed_txt >= 0:
            try:
                subject_dic[self.subject_menu.get()]['lecture_dic'][self.lecture_menu.get()] = copy.deepcopy(self.undoredo[needed_txt])
                self.undoredo_idx = needed_txt
                self.change_lecture(resetundoredo=False)
                # Wipe out the text window and insert the text from the undo/redo stack.
            except IndexError:
                pass

        return 'break'


    def resetundoredo(self, *args):
    # This function wipes out the undoredo stack when the lecture is changed. It also creates the undoredo stack when launching the application.
        subject_id = self.subject_menu.get()
        lecture_id = self.lecture_menu.get()
        self.undoredo = [copy.deepcopy(subject_dic[subject_id]['lecture_dic'][lecture_id])]
        self.undoredo_idx = 0


    def download(self, what):

        '''Download the current lecture, every lecture in the subject or every subject and
        their lecture to the selected directory.'''

        def text_for_download(subject_id, lecture_id):
            '''Inserts the text in the hidden_text for the application to parse through for a creation
            of a new word document without disrupting whatever user is doing.'''
            lecture = subject_dic[subject_id]['lecture_dic'][lecture_id]
            self.text_hidden.delete(1.0, 'end')
            self.text_hidden.insert(1.0, lecture['text'])
            self.text_hidden.edit_reset()

            for i in lecture:
                if i.startswith('tag_#'):
                    tag = i
                    for n, start_idx in enumerate(lecture[tag]['begin']):
                        end_idx = lecture[tag]['end'][n]
                        self.text_hidden.tag_add(tag, start_idx, end_idx)
                    for tagconfig in self.all_tag_configs:
                        value = lecture[tag][tagconfig]
                        self.text_hidden.tag_configure(tag, **{tagconfig: value})


        if what == 'lecture':
            subject = self.subject_menu.get()
            lecture = self.lecture_menu.get()
            directory = filedialog.asksaveasfilename(parent=self.subject_menu.win, 
                title='Choose where to save the file...', defaultextension='.docx', 
                filetypes=[('Word files', '*.docx')], initialfile=subject_dic[subject]['lecture_dic'][lecture]['name'])

            if directory:
                WordDoc(self.text, directory, self.all_tag_configs)

        else:
            self.text_hidden = ImprovedText(self.main_three, relief='flat', wrap='word', tabs='17m',
                font=['Calibri', 12], selectbackground='SystemHighlight', inactiveselectbackground='SystemHighlight', height=30)
            self.text_hidden.grid(column=0, row=0, sticky='NSEW')
            self.text_hidden.lower()
            # For downloading the whole subject or everything, we hide another text window that will store those multiple texts
            # that need to be parsed through to create the Word file. 

            directory = filedialog.askdirectory(parent=self.subject_menu.win, 
                title='Choose where to create a folder for the contents of the subject...')
            if directory:
                if what == 'subject':
                    subject_id = self.subject_menu.get()
                    subject_name = subject_dic[subject_id]['name']
                    folder_directory = directory + f'/{subject_name}'
                    os.mkdir(folder_directory)

                    for lecture_id in subject_dic[subject_id]['lecture_dic']:
                        text_for_download(subject_id, lecture_id)
                        lecture_name = subject_dic[subject_id]['lecture_dic'][lecture_id]['name']
                        directory = folder_directory + f'/{lecture_name}.docx' 
                        WordDoc(self.text_hidden, directory, self.all_tag_configs)
                elif what == 'everything':
                    folder_directory = directory + f"/Student's Text Editor"
                    os.mkdir(folder_directory)

                    for subject_id in subject_dic:
                        subject_name = subject_dic[subject_id]['name']
                        subfolder_directory = folder_directory + f'/{subject_name}'
                        os.mkdir(subfolder_directory)

                        for lecture_id in subject_dic[subject_id]['lecture_dic']:
                            text_for_download(subject_id, lecture_id)
                            lecture_name = subject_dic[subject_id]['lecture_dic'][lecture_id]['name']
                            directory = subfolder_directory + f'/{lecture_name}.docx' 
                            WordDoc(self.text_hidden, directory, self.all_tag_configs)

            self.text_hidden.grid_forget()


    def reset(self, *args):

        '''Deletes every subject and in turn all of the lectures.'''

        yes = messagebox.askokcancel(message='Reseting the application will remove all of its contents.\nAre you sure you would like to proceed?',
            title='Application Reset', default='cancel', parent=self.subject_menu.win)
        
        if yes:
            for button in self.subject_menu.menu.grid_slaves():
                self.subject_menu.mouse_button = button
                self.subject_menu.delete()
            

################################# CLASSES
################## TEXT FIELD CLASS AND BASE CLASSES

class ImprovedText(tk.Text):
    '''The improvement to the original Text widget is thus, that it will now
    generate a <<CursorChange>> event whenever the "insert" mark/cursor
    changes its position, <<TextInsert>> event whenever a text is
    inserted and <<TextDelete>> whenever the text is deleted.
    Thanks: https://stackoverflow.com/questions/23571407/how-to-i-have-the-call-back-in-tkinter-when-i-change-the-current-insert-position'''

    def __init__(self, *args, **kwargs):
        tk.Text.__init__(self, *args, **kwargs)

        # Create a proxy for the underlying widget
        self._orig = self._w + "_orig"
        self.tk.call("rename", self._w, self._orig)
        self.tk.createcommand(self._w, self._proxy)


    def _proxy(self, *args):
        cmd = (self._orig,) + args
        result = self.tk.call(cmd)

        # Generate an event if something was added or deleted,
        # or the cursor position changed.
        if args[0] in ("insert", "delete"):
            self.event_generate("<<TextInsert>>", when="tail")
        if args[0] in ("delete"):
            self.event_generate("<<TextDelete>>", when="tail")
        if (args[0] in ("insert", "delete") or 
            args[0:3] == ("mark", "set", "insert")):
            self.event_generate("<<CursorChange>>", when="tail")

        return result


class PopupWindow(tk.Toplevel):

    '''Toplevel widget with some extra default configurations that makes it look like a popup and in turn becomes very versatile.'''

    def __init__(self, *args, transient=True, **kwargs):
        tk.Toplevel.__init__(self, *args, **kwargs)

        self.state('withdrawn')
        self.overrideredirect(1)
        if transient:
            self.transient(root)


    def close(self, button_func=None, caller=None, *args):
        ''' Although it is built as if it could, Tkinter is actually unable to unbind a specific command (https://bugs.python.org/issue31485)
            Thus, I need to bind the closing "after" 10ms (1ms also works, but I'm keepin' it safe) so that there
            is time for unbind to fire first, because otherwise it will fire only after opening another widget that uses this func.'''
        if self.state() == 'normal':
            root.after(10, root.bind, '<ButtonRelease-1>', lambda e: self.helper(button_func, caller), "+")
            root.after(10, root.bind, '<ButtonRelease-3>', lambda e: self.helper(button_func, caller), "+")
        else:
            root.unbind('<ButtonRelease-1>')
            root.unbind('<ButtonRelease-3>')


    def helper(self, button_func, caller):

        if root.winfo_containing(root.winfo_pointerx(), root.winfo_pointery()) != caller:
            button_func()


class ButtonTxt(tk.Button):

    '''Button class that other button classes which do not use images will build on.'''

    def __init__(self, *args, func, **kwargs):
        tk.Button.__init__(self, *args, command=self.press, width=3, height=1, activebackground='#ECF2F5', 
            activeforeground='#0457B6', background='white', borderwidth=0, relief='flat', 
            cursor='hand2', **kwargs)       

        self.pressed = False
        self.func = func

        self.bind('<Enter>', lambda e: self.configure(background='#EBEDEE'))
        self.bind('<Leave>', self.leave)


    def press(self, *args):
    # This function will be overwritten in all subclasses.
        pass


    def leave(self, *args):

        if self.pressed:
            self.configure(background='#ECF2F5')
        else:
            self.configure(background='white')


class ButtonImg(tk.Button):

    '''Button class that other button classes which use images will build on.'''

    def __init__(self, *args, images, func, **kwargs):
        self.img1 = images[0]
        self.img2 = images[1]
        self.img3 = images[2]
        tk.Button.__init__(self, *args, image=self.img1, command=self.press, width=35, height=36, 
            activebackground='#ECF2F5', background='white', borderwidth=0, relief='flat', 
            cursor='hand2', **kwargs)
        
        self.pressed = False
        self.func = func

        self.bind('<ButtonPress-1>', lambda e: self.configure(image=self.img3))
        self.bind('<Enter>', self.enter)
        self.bind('<Leave>', self.leave)
        

    def press(self, *args):
    # This function will be overwritten on all ButtonImg subclasses.
        pass


    def enter(self, *args):

        if self.pressed:
            self.configure(background='#EBEDEE')
        else:
            self.configure(background='#EBEDEE', image=self.img2)


    def leave(self, *args):

        if self.pressed:
            self.configure(background='#ECF2F5')
        else:
            self.configure(background='white', image=self.img1)


################## MENU CLASSES
class SubjectMenu(ButtonTxt):

    def __init__(self, *args, caller, **kwargs):

        '''The subject menu is built on a Popupwindow that is attached to the left side of the root window.
        By nestting frames and a canvas inside it, I was able to create a scrollable widget containing individual
        buttons that act as individual subjects.'''

        ButtonTxt.__init__(self, *args, **kwargs)

        self.caller = caller
        self.shortened_names = []
        self.mouse_button = None
        
        self.win = PopupWindow(root)
        # Creating a TopLevel window so that it is possible to bind a mousewheel for scrolling.
        self.add_button = AddButton(self.win, func=self.caller.add_subject)
        self.add_button.configure(font=['Calibri', '11', 'bold'], width=6)
        self.add_button.pack(pady=(10, 0), padx=(10, 0))
        self.frame = tk.Frame(self.win)
        self.frame.pack(pady=10, padx=(10, 0))

        self.canvas = tk.Canvas(self.frame, width=150, height=502, highlightthickness=0, background='white')
        self.menu = tk.Frame(self.canvas)
        self.scrollbar = ttk.Scrollbar(self.frame, orient='vertical',
            command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.scrollbar.set)

        self.scrollbar.pack(side="right", fill="y")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas.create_window((0, 0), window=self.menu, anchor="nw")

        root.update()
        self.y = lambda: self.caller.text.winfo_height()
        # This is used to adjust the height of the win and canvas accordingly to the height of the text 
        # widget when its being expanded or shrunken by the user.
        self.geometry = lambda: f'150x{str(int(self.y())+47)}+{str(int(root.winfo_x()-142))}+{self.winfo_rooty()}'
        root.bind('<Configure>', self.attach, '+')
        root.bind('<Map>', lambda e: self.win.lift())
        # Another binding to help the window stay on top of the root window or it will disappear if 
        # the root window gets maximized or minimized.
        self.bind('<Leave>', lambda e: self.configure(background='white'))
        # Need to overwrite the ButtonTxt binding, because this button does not stay
        # cosmetically pressed after pressing.
        self.press()


    def attach(self, *args):

        ''' Attaching the window to the root screen and making it stay there.
            It will disappear if not lifted when some other window gets on top of the Application window.'''

        self.win.geometry(self.geometry())
        self.canvas.configure(height=self.y())
        self.update(None, update=False)
        self.win.lift()


    def press(self, *args):

        if self.pressed:
            self.configure(text=u"\u25C0")
            self.win.state('withdrawn')
            self.pressed = False
        else:
            self.configure(text=u"\u25B6")
            self.win.state('normal')
            self.attach()
            self.pressed = True


    def insert(self, subject_id, press=True):

        '''Inserts a subject button.'''

        position = self.menu.grid_size()[1]
        item = ButtonSubject(self.menu, caller=self, id_=subject_id, position=position, func=self.func, text=subject_dic[subject_id]['name'], anchor='w')
        item.shorten_name()
        item.grid(row=position, sticky='WE')
        if press:
            item.press()
        self.sort(see=False)

        self.update(item)


    def create_entry(self, call, first_time=False):
        
        '''Creates an entry for the user to type in a new subject name or a new name for an existing subject.'''

        if call == 'rename':
            empty_row = self.mouse_button.position
            self.mouse_button.grid_remove()
            self.entry = EntryMenu(self.menu, caller=self, position=empty_row)
            name = subject_dic[self.mouse_button.id]['name']
            self.entry.insert(0, name)
            self.entry.oldcontents = self.entry.get()
        elif call == 'add_new':
            if not first_time:
                self.get(self.get()).unpress()
            empty_row = self.menu.grid_size()[1]
            self.entry = EntryMenu(self.menu, caller=self, position=empty_row)
            self.entry.insert(0, self.check_duplicate('Subject'))

        self.entry.grid(row=empty_row, sticky='W')
        self.entry.selection_range(0, 'end')

        self.update(self.entry)

        return self.entry


    def get(self, subject_id=None, *args, position=None):

        '''If the id or position is given, returns the tkinter name for the button. 
            Else - returns the currently pressed button's id.'''

        for button in self.menu.grid_slaves():
            if subject_id:
                if button.id == subject_id:
                    return button
            elif position or position == 0:
                if button.position == position:
                    return button
            elif button.pressed:
                return button.id


    def get_mouse(self, remember_button=True):

        '''Gets the subject button that the mouse was hovering over.'''

        mouse_button = root.winfo_containing(root.winfo_pointerx(), root.winfo_pointery())
        if remember_button:
            self.mouse_button = mouse_button
        
        return mouse_button


    def set(self, subject_id=None, previous=False):

        '''Sets the button of the given id.'''

        button = self.get(subject_id)
        button.press()
        self.update(button)


    def check_duplicate(self, contents):

        '''Checks for the duplicate names.'''

        for d in subject_dic.values():
            name = d['name']
            if name == contents:
                if contents[0] == '(' and contents[2] == ')':
                    contents = f'({str(int(contents[1])+1)})' + contents[3:]
                else:
                    contents = '(1)' + contents

        return contents


    def delete(self):

        global subject_dic

        subject_id = self.mouse_button.id
        del subject_dic[subject_id]

        current_id = self.get()
        if self.mouse_button.id == current_id:
            self.mouse_button.unpress()
            current_button = True
        else:
            current_button = False

        empty_row = self.mouse_button.position
        self.mouse_button.grid_forget()

        for button in self.menu.grid_slaves():
            if button.position > empty_row:
                button.grid_forget()
                button.grid(row=(button.position -1), sticky='WE')
                button.position -= 1

        if current_button:
            button = self.get(position=empty_row)
            if button:
                button.press()
            elif self.menu.grid_size()[1] == 0:
                self.caller.add_subject(first_time=True)
            else:
                button = self.get(position=empty_row -1)
                button.press()

        self.update(None)
        self.caller.save()


    def sort(self, sort_variable=None, save=False, see=True):

        global sorting_info

        direction = sorting_info[1]
        change_direction = False
        if sort_variable:
            if sort_variable == sorting_info[0]:
                change_direction = True
        else:
            sort_variable = sorting_info[0]

        if sort_variable == 'date_created':
            button_dic = {button.date_created: button for button in self.menu.grid_slaves()}
            button_list = list(button_dic.keys())
            button_list.sort()
            if change_direction:
                if direction == 'newest':
                    direction = 'oldest'
                else:
                    direction = 'newest'
            if direction == 'newest':
                button_list = button_list[::-1]
            else:
                direction = 'oldest'
        elif sort_variable == 'name':
            button_dic = {button.cget('text'): button for button in self.menu.grid_slaves()}
            button_list = list(button_dic.keys())
            button_list.sort()
            if change_direction:
                if direction == 'Z to A':
                    direction = 'A to Z'
                else:
                    direction = 'Z to A'
            if direction == 'Z to A':
                button_list = button_list[::-1]
            else:
                direction = 'A to Z'

        sorting_info = [sort_variable, direction]

        for n, key in enumerate(button_list):

            button = button_dic[key]
            button.grid_forget()
            button.grid(row=n)
            button.position = n

        if see:
            self.see(self.get(self.get()))

        if save:
            self.caller.save()


    def update(self, item, *args, update=True):

        '''Updates the canvas so that newly inserted button can be scrolled to and automatically
        scrolls to the button for the user when it gets inserted or selected, etc.'''

        if update:
            root.update()
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))
        if item:
            self.see(item)

        if self.scrollbar.get() != (0.0, 1.0):
            self.win.bind("<MouseWheel>", lambda e: self.canvas.yview_scroll(int(-1*(e.delta/120)), "units"))
        else:
            self.win.unbind("<MouseWheel>")
            # If scrollbar is bound before there is anything to scroll, it will still be able to scroll, but it will scroll to the nothingness.


    def see(self, item):
        
        '''Getting scrollbar to move so that the user can see the selected widget.'''
        
        scroll_offset_y1 = self.scrollbar.get()[0] # Top y in percentage
        scroll_offset_y2 = self.scrollbar.get()[1] # bottom y
        canvas_y = self.canvas.bbox('all')[3] # Canvas y in pixels

        item_height = item.winfo_height()
        if item_height < canvas_y / self.menu.grid_size()[1]:
            # Entry has a smaller size than a button so we need to adjust for that.
            for widget in self.menu.grid_slaves():
                widget_height = widget.winfo_height()
                if widget_height > item_height:
                    item_height = widget_height

        item_y = (item.position + 1) * item_height # Item's bottom y in pixels
        if item_y < canvas_y / 2:
            item_y -= item_height
            # If item is at the top half of the canvas, we need it's top y.
        item_offset_y = item_y / canvas_y # Item's bottom y in percentage
        
        if item_offset_y > scroll_offset_y2 or item_offset_y < scroll_offset_y1:
            self.canvas.yview_moveto(item_offset_y)


class ButtonSubject(ButtonTxt):


    def __init__(self, master, *args, caller, id_, position, **kwargs):
        ButtonTxt.__init__(self, master, *args, **kwargs)

        self.master = master
        self.caller = caller
        self.id = id_
        self.position = position
        self.date_created = datetime.strptime(subject_dic[self.id]['date_created'], '%Y-%m-%d %H:%M:%S')
        self.shorter_name = None
        self.configure(font=['Calibri', '11'], width=17, pady=3)

        self.popup = PopupWindow(root, transient=False) 
        self.popup_label = ttk.Label(self.popup)
        self.popup_label.pack()

        self.bind('<Enter>', self.pre_popup, "+")
        self.bind('<Leave>', lambda e: self.popup.state('withdrawn'), "+")


    def press(self, *args, run_func=True):

        if not self.pressed:
            for button in self.master.grid_slaves():
                if button.pressed:
                    button.unpress()
                    break

            self.configure(background='#ECF2F5', foreground='#0457B6')
            self.pressed = True
            if run_func:
                self.func()


    def unpress(self, *args):

        self.configure(background='white', foreground='black')
        self.pressed = False


    def shorten_name(self):
        '''Shortens the name of the subject if the text does not fit in the width of the
        parent screen and saves into a button attribute so that it can be reffered to by the popup. I found 8.82 to be a good ratio
        when converting width from pixels to characters (different widgets take different units to set width, unfortunately).'''
        current_name = self.cget('text')
        available_length = int(int(self.caller.canvas.cget('width'))/8.82)
        if len(current_name) >= available_length:
            shorter_name = current_name[:available_length - 2] + '...'
            self.configure(text=shorter_name)
            self.shorter_name = shorter_name


    def pre_popup(self, *args):

        if self.popup.state() != 'normal':
            self.after(800, self.create_popup) 


    def create_popup(self, *args):

        '''Creates a popup that displays information about the subject.'''

        button = self.caller.get_mouse(remember_button=False)

        if button == self:
            self.popup.state('normal')
            self.popup.lift()

            name = ''
            if self.shorter_name:
                name = subject_dic[self.id]['name'] + '\n'
            text = name + f'Date created: {subject_dic[self.id]["date_created"]}'
            self.popup_label.configure(text=text)
            self.popup.update() #Popup needs to be updated so that it transforms
                                #to the size of the Label.

            popup_size = str(self.popup.geometry()).split('+')[0]
            x = root.winfo_pointerx()+10
            y = root.winfo_pointery()+17
            self.popup.geometry(f'{popup_size}+{x}+{y}')
            self.popup.update()


class LectureMenu(tk.Frame):
    def __init__(self, *args, caller, **kwargs):

        '''Lecture menu is essentially a frame that holds buttons. This combined with a
         Popupwindow class is able to create a movable tabs experience. More on that
         in the ButtonLecture class.'''

        tk.Frame.__init__(self, *args, width=600, height=32)

        self.caller = caller
        self.subject_menu = self.caller.subject_menu
        self.mouse_button = None
        self.wide_buttons = []
        self.active_tab = False
        # This is needed to control the adjust_width from getting out of hand when the tabs are being moved.

        self.add_button = AddButton(self, func=self.caller.add_lecture)
        self.menu = tk.Frame(self, width=588, height=32)

        self.add_button.grid(column=1, row=0, sticky='W')
        self.menu.grid(column=0, row=0, sticky='E')
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        self.width = lambda: int(self.winfo_width()) - int(self.add_button.winfo_width())
        # This attribute changes if the window is being resized, thus it will let us fill up the
        # empty space of expanded window on adjust_width() funcion.
        self.after(3000, lambda: self.bind('<Configure>', lambda e: self.adjust_width(), '+'))
        # self.bind('<Configure>', self.resize)
        # The application loads faster and smoother if this binding is not immediatelly in effect.
        # This binding resizes the self.menu when the whole application window is being resized.


    def insert(self, lecture_id, press=True):

        '''Inserts a new button that acts as an individual lecture.'''

        subject_id = self.subject_menu.get()
        position = self.menu.grid_size()[0]
        item = ButtonLecture(self.menu, caller=self, id_=lecture_id, position=position, func=self.caller.change_lecture, text=subject_dic[subject_id]['lecture_dic'][lecture_id]['name'], anchor='w')
        item.grid(column=position, row=0, sticky='N, S')
        item.grid_columnconfigure(position, weight=1)
        self.adjust_width()
        if press:
            item.press()


    def adjust_width(self, entry=0, expand=None):

        '''Controls the width of the buttons in the Lecture menu. This makes them expand when hovered over or pressed.'''

        slaves = self.get_order(buttons=True)
        # grid_slaves() gives a disordered list (not by their column/row position in grid). After moving tabs, this becomes very apparent.
        entry_is_not_active = True
        for i in slaves:
            if '!entrymenu' in str(i):
                entry_is_not_active = False

        if entry_is_not_active:
        # As the adjust_width function gets called pretty often and having an active entry while it runs
        # causes multiple exceptions, I came to a decision to do an entry-check before running it.
            if entry:
                entry = self.entry.winfo_reqwidth()

            # menu_width = int(self.menu.cget('width')) - entry
            menu_width = self.width() - entry
            if menu_width > 900:
            # We are not letting the lecture menu expand uncontrollably.
                menu_width = 900
            button_num = self.menu.grid_size()[0]
        
            pressed = self.get(self.get())
            if button_num > 7 and not entry and not self.active_tab and pressed:        
            # If there are many buttons, for user-convenience purposes, the button that is pressed
            # and the nearest buttons will be made bigger while all the others - smaller.
            # If the expand variable is given, this will expand the button that the mouse is over (and its neighbors).

                def adjust(slaves, wide_buttons):

                    for button in slaves:
                        if button in wide_buttons:
                            button.configure(width=wide_button_width)
                        else:
                            button.configure(width=button_width)
                        button.shorten_name()

                    self.wide_buttons = wide_buttons
                    root.update()

                wide_buttons = []

                wide_button_width = int(((menu_width * 3/4) /5) /8.82)
                button_width = int(((menu_width * 1/4) / (button_num-5)) /8.82)

                mouse = self.get_mouse(remember_button=False)
                if str(self) in str(mouse):

                    if expand:
                        main = expand
                        pos = expand.position
                    else:
                        main = pressed
                        pos = pressed.position  

                    root.bind('<Motion>', self.motion_width)

                    wide_buttons.append(main)
                    higher_pos = iter([button.position for button in slaves if button.position > pos])
                    smaller_pos = iter([button.position for button in slaves if button.position < pos][::-1])
                    while len(wide_buttons) < 5:
                        try:
                            wide_buttons.append(self.get(position=next(higher_pos)))
                        except StopIteration:
                            wide_buttons.append(self.get(position=next(smaller_pos)))

                    adjust(slaves, wide_buttons)

                    wide_positions = [button.position for button in wide_buttons]
                    wide_positions.sort()
                    wide_buttons = [self.get(position=pos) for pos in wide_positions]

                    mouse = self.get_mouse(remember_button=False)
                    while str(mouse) != str(main):
                        try:
                            p = self.get(position=next(smaller_pos))
                            wide_buttons.pop(-1)
                            wide_buttons.insert(0, p)
                            adjust(slaves, wide_buttons)
                            mouse = self.get_mouse(remember_button=False)
                        except StopIteration:
                            try:
                                p = self.get(position=next(higher_pos))
                                wide_buttons.pop(0)
                                wide_buttons.append(p)
                                adjust(slaves, wide_buttons)
                                mouse = self.get_mouse(remember_button=False)
                            except StopIteration:
                                # Sometimes for a weird reason related to the mouse cursor being on top of the Lecture Menu
                                # widget while it is loading, this function goes into an infinite StopIteration exception loop
                                # so we need to break out of it.
                                break

                else:
                    # A more user friendly way to adjust button width when there is no need to have a selected button below the mouse.
                    pos = pressed.position
                    slaves = slaves[::-1] #This is needed for the adjustment of position "1" below to be relevant.
                    for button in slaves:
                        if button.position in range(pos-2, pos+3):
                            wide_buttons.append(button)

                    if pos == 1:
                    # There is this one particular case when the code below does not give the needed result,
                    # thus we adjust for it individually here.
                        wide_buttons.append(self.get(position=4))

                    buttons = iter([button for button in slaves if button not in wide_buttons and button.position in range(pos-4, pos+5)])
                    while len(wide_buttons) < 5:
                        button = next(buttons)
                        wide_buttons.append(button)
                    
                    adjust(slaves, wide_buttons)
                    

            elif button_num > 2:
                # Giving all the buttons an equal width.
                button_width = int((menu_width /button_num) /8.82)
                for button in slaves:
                    button.configure(width=button_width)
                    button.shorten_name()
            else:
                # When there are less than 3 buttons, I need to limit their side or they will look ridiculous.
                for button in slaves:
                    button.configure(width=26)
                    button.shorten_name(available_length=26)


    def motion_width(self, *args):

        '''Controls when to adjust the button width to a more user friendly way'''

        mouse_location = self.get_mouse(remember_button=False)
        if str(self) not in str(mouse_location):
            root.unbind('<Motion>')
            self.adjust_width()


    def get(self, lecture_id=None, *args, position=None):

        ''' If the id or position is given, returns the tkinter name for the button. 
        Else - returns the currently pressed button's id.'''

        for button in self.menu.grid_slaves():
            if lecture_id:
                if button.id == lecture_id:
                    return button
            elif position or position == 0:
                if button.position == position:
                    return button
            elif button.pressed:
                return button.id


    def get_mouse(self, remember_button=True):

        '''Gets the lecture button that the mouse was hovering over.'''

        mouse_button = root.winfo_containing(root.winfo_pointerx(), root.winfo_pointery())
        if remember_button:
            self.mouse_button = mouse_button
        
        return mouse_button


    def get_order(self, buttons=False):

        '''Orders the grid.slaves by their grid positions.'''

        if buttons:
            ord_dic = {button.position: button for button in self.menu.grid_slaves()}
        else:
            ord_dic = {button.position: button.id for button in self.menu.grid_slaves()}
        positions = list(ord_dic.keys())
        positions.sort()
        order = [ord_dic[position] for position in positions]

        return order


    def set(self, lecture_id=None):

        '''Sets the button of the given id.'''

        button = self.get(lecture_id)
        button.press()


    def check_duplicate(self, contents):

        '''Checks for the duplicate names including the default name.'''

        subject_id = self.subject_menu.get()
        lecture_dic = subject_dic[subject_id]['lecture_dic']
        for d in lecture_dic.values():
            name = d['name']
            if name == contents:
                if name.startswith('Lecture ') and name[8] in [str(n) for n in range(10)]: #Entry-level check.
                    num = name[8:]
                    contents = contents[:8] + str(int(num) +1)
                else: #Regular check.
                    if contents[0] == '(' and contents[2] == ')':
                        contents = f'({str(int(contents[1])+1)})' + contents[3:]
                    else:
                        contents = '(1)' + contents

        return contents


    def create_entry(self, call):

        '''Creates an entry for the user to type in a name for a new or renamed lecture.'''
        
        if call == 'rename':
            empty_col = self.mouse_button.position
            self.mouse_button.grid_remove()
            subject_id = self.subject_menu.get()
            name = subject_dic[subject_id]['lecture_dic'][self.mouse_button.id]['name']
            self.entry = EntryMenu(self.menu, caller=self, position=empty_col)
            self.entry.insert(0, name)
            self.entry.oldcontents = self.entry.get()
        elif call == 'add_new':
            self.get(self.get()).unpress()
            empty_col = self.menu.grid_size()[0]
            self.entry = EntryMenu(self.menu, caller=self, position=empty_col)
            self.entry.insert(0, self.check_duplicate('Lecture 1'))

        self.adjust_width(entry=True)
        self.entry.grid(column=empty_col, row=0, sticky='W')
        self.entry.selection_range(0, 'end')

        return self.entry


    def delete(self):

        global subject_dic

        subject_id = self.subject_menu.get()
        lecture_id = self.mouse_button.id

        del subject_dic[subject_id]['lecture_dic'][lecture_id]

        current_id = self.get()
        if self.mouse_button.id == current_id:
            self.mouse_button.unpress()
            current_button = True
        else:
            current_button = False

        empty_col = self.mouse_button.position
        self.mouse_button.grid_forget()

        for button in self.menu.grid_slaves():
            if button.position > empty_col:
                button.grid_forget()
                button.grid(column=(button.position -1), row=0, sticky='WE')
                button.position -= 1

        if current_button:
            button = self.get(position=empty_col)
            if button:
                button.press()
            elif self.menu.grid_size()[0] == 0:
                # We cannot leave the option for the user to not have at least one lecture 
                # for one subject, thus we automatically create one similarlty like in add_subject function.
                self.caller.new_lecture('Lecture 1', fake_text=False)
                lecture_id = self.get()
                self.mouse_button = self.get(lecture_id)
                self.caller.rename(menu=self)
                return None
                # We need to brake out of the funcion as we do not want an adjust_width to run having an active entry.
            else:
                button = self.get(position=empty_col -1)
                button.press()

        self.adjust_width()
        self.caller.save()


class ButtonLecture(ButtonTxt):

    def __init__(self, master, *args, caller, id_, position, **kwargs):

        '''The button that is an individual lecture. It also gets an inidividual tab window that appears when the user
        starts the draggin motion.'''

        ButtonTxt.__init__(self, master, *args, **kwargs)

        self.master = master
        self.caller = caller
        self.id = id_
        self.position = position
        self.shorter_name = None
        self.configure(font=['Calibri', '12'], relief='ridge', borderwidth=1, padx=3)
        self.bind('<3>', self.caller.caller.post_lecture)
        # As it is impossible to bind mouse actions on a tkinter frame that does not
        # have focus and I am not going to give it focus, I need to bind the contextual right lick menu on each button individually.        

        self.popup = PopupWindow(root, transient=False) 
        self.popup_label = ttk.Label(self.popup)
        self.popup_label.pack()
        self.bind('<Enter>', self.pre_popup, "+")
        self.bind('<Leave>', lambda e: self.popup.state('withdrawn'), "+")

        self.tab = PopupWindow(root)
        self.tab_label = tk.Label(self.tab, relief='ridge', background='#ECF2F5', foreground='#0457B6', borderwidth=1, padx=3, font=['Calibri', '12'], text=subject_dic[self.caller.subject_menu.get()]['lecture_dic'][self.id]['name'])
        self.tab_label.pack(expand=True, fill='both')
        self.boundary_left = lambda: self.master.winfo_rootx() + int(self.tab.cget('width')/2)
        self.boundary_right = lambda: self.master.winfo_rootx() + self.master.winfo_width() - int(self.tab.cget('width')/2) 
        self.y = lambda: self.master.winfo_rooty() + 1 
        self.size = lambda: f'{self.winfo_width()}x{self.winfo_height()}'
        self.geometry = lambda: f'{self.size()}+{self.x()}+{self.y()}'
        self.bind('<B1-Motion>', self.drag)


    def x(self):

        '''The X axis and its boundaries that the tab can move in.'''

        l = self.boundary_left() 
        r = self.boundary_right()
        x = root.winfo_pointerx()
        if x > l and x < r:
            return x - int(self.tab.winfo_width() /2) # Middle of the mouse
        elif x <= l:
            return l
        elif x >= r:
            return r


    def press(self, *args, run_func=True, adjust_width=True):

        if not self.pressed:
            for button in self.master.grid_slaves():
                if button.pressed:
                    button.unpress()
                    break

            self.configure(background='#ECF2F5', foreground='#0457B6')
            self.pressed = True
            if adjust_width:
                self.caller.adjust_width()
            if run_func:
                self.func()


    def unpress(self, *args):

        self.configure(background='white', foreground='black')
        self.pressed = False


    def shorten_name(self, available_length=None):

        '''Does not actually shorten the name as it was decided against that for the 
        lecture_menu population, however, it is still needed for the popups.'''

        subject_id = self.caller.subject_menu.get()
        current_name = subject_dic[subject_id]['lecture_dic'][self.id]['name']
        if not available_length:
            available_length = self.cget('width')
        if len(current_name) >= available_length:
            self.shorter_name = True
        else:
            self.shorter_name = False


    def pre_popup(self, *args):

        if self.popup.state() != 'normal' and self.tab.state() != 'normal':
            self.after(800, self.create_popup) 


    def create_popup(self, *args):

        '''Creates a popup that displays information about the lecture.'''

        button = self.caller.get_mouse(remember_button=False)

        if button == self:

            if button not in self.caller.wide_buttons:
                self.caller.adjust_width(expand=button)

            self.popup.state('normal')
            self.popup.lift()

            subject_id = self.caller.subject_menu.get()
            lecture = subject_dic[subject_id]['lecture_dic'][self.id]

            name = ''
            if self.shorter_name:
                name = lecture['name'] + '\n'
            text = name + f'Last modified: {lecture["last_modified"]}\nDate created: {lecture["date_created"]}'
            self.popup_label.configure(text=text)
            self.popup.update() #Popup needs to be updated so that it transforms
                                #to the size of the Label.

            popup_size = str(self.popup.geometry()).split('+')[0]
            x = root.winfo_pointerx()+10
            y = root.winfo_pointery()+17
            self.popup.geometry(f'{popup_size}+{x}+{y}')
            self.popup.update()


    def drag(self, *args):

        '''When the draggin motion initiates, the tab window appears and the lecture button becomes white, its
        name invisibe, representing the empty space. It moves together with the tab window changing its grid
        and other button grids according to the tab window's position. When the motion stops, the tab is hidden
        and the button becomes not blank again standing in its new place.'''
        
        if self.master.grid_size()[0] > 1:
            if self.tab.state() == 'withdrawn':
                self.bind('<ButtonRelease-1>', self.stop_drag, '+')

                self.press(adjust_width=False)
                # The dragged button is of user focus. Because of this and other complications,
                # the dragged button gets pressed as is the case in any other application that uses tabs.
                # Note: adjusting_width messes things up.
                self.caller.active_tab = True
                self.caller.adjust_width()
                self.tab.state('normal')
                self.tab.lift()
                self.tab.update()
                self.tab.geometry(self.geometry())

            else:
                self.tab.geometry(self.geometry())
                self.configure(fg='white', background='white', borderwidth=0)
                # For a strange reason the invisible buttons backgroundg becomes blue 
                # again after hitting against right boundry so we need to reconfigure it every time just in case.
                below_widget = root.winfo_containing(root.winfo_pointerx(), self.master.winfo_rooty())

                for button in self.master.grid_slaves():

                    if str(below_widget) == str(button):
                        oldposition = self.position
                        self.position = below_widget.position
                        below_widget.position = oldposition

                        self.grid(column=self.position, row=0)
                        below_widget.grid(column=below_widget.position, row=0)
                        break


    def stop_drag(self, *args):
        
        self.tab.state('withdrawn')
        self.configure(background='#ECF2F5', foreground='#0457B6', borderwidth=1)
        self.caller.active_tab = False
        self.caller.adjust_width()
        self.tab.bind('<Enter>', self.stop_drag)
        self.tab.bind('<Leave>', self.stop_drag)
        # These two bindings prevent the situation of when the <ButtonRelease> binding
        # is not enough to get rid of the tab (for example when tab is moved very quickly).
        self.caller.caller.save()


class AddButton(ButtonTxt):

    '''The button with a "+" sign that adds new Subject or Lecture.'''

    def __init__(self, *args, **kwargs):
        ButtonTxt.__init__(self, *args, **kwargs)

        self.configure(font=['Calibri', '12', 'bold'], text='+', width=8, relief='raised', borderwidth=1)


    def press(self, *args):

        self.func()


class EntryMenu(tk.Entry):

    '''An Entry widget used by both Subject and Lecture menus.'''

    def __init__(self, *args, caller, position=None, **kwargs):
        tk.Entry.__init__(self, *args, relief='flat', 
            highlightcolor=None, highlighbackground=None, highlightthickness=0,
            background='#ECF2F5', exportselection=False, borderwidth=0, 
            font='Calibri 13', width=14, justify='center', **kwargs)

        self.pressed = False
        # Entry appears in the list of buttons in the self.menu of the menu widget. To not interefere
        # with the commands that itterate through the buttons, let's set it as if it is not pressed.
        self.caller = caller
        self.position = position
        self.oldcontents = ''
        self.focus()


    def validate(self, call, func1, func2=None):

        contents = self.get()
        if call == 'rename':
            if contents != '' and contents != self.oldcontents:
                self.grid_forget()
                self.caller.mouse_button.grid()
                func1(contents=self.caller.check_duplicate(contents), menu=self.caller)
            elif func2:
                func2(menu=self.caller)
        elif call == 'add_new':
            if contents != '':
                self.grid_forget()
                func1(contents=self.caller.check_duplicate(contents))
            elif func2:
                func2(menu=self.caller)


####################### TEXT AND PARAGRAPH STYLING WIDGETS.
########## ButtonTxt subclasses.

class ButtonStyle(ButtonTxt):

    '''ButtonTxt sublass used for bold/italic/underline.'''

    def __init__(self, *args, **kwargs):
        ButtonTxt.__init__(self, *args, **kwargs)


    def press(self, *args, set_state=None, run_func=True):

            if set_state:
            # If there some specific state of the button is requested, we need to set the
            # pressed variable to the opposite state.
                if set_state == 'pressed':
                    self.pressed = False
                elif set_state == '!pressed':
                    self.pressed = True
                run_func = False            

            if run_func:
                self.func()

            if self.pressed:
                self.configure(background='white', foreground='black')
                self.pressed = False
            else:
                self.configure(background='#ECF2F5', foreground='#0457B6')
                self.pressed = True

            return 'break'
            # Needed to overwrite some Tk default bindings (e.g. for <Ctrl+i>).


class ButtonMenu(ButtonTxt):

    '''A button representing an inidividual item in a dropdown menu. Used in all the dropdown widgets below.'''

    def __init__(self, master, *args, name, caller_button, type_=None, **kwargs):
        ButtonTxt.__init__(self, master, *args, **kwargs)

        self.master = master
        self.caller_button = caller_button
        self.name = name
        self.type = type_
        if self.type == 'font':
            self.configure(width=15, font=[self.name, '12'])
        elif self.type == 'size':
            self.configure(font=['Calibri', '12'])
        else:
            self.configure(width=10, font=['Calibri', '12'])


    def press(self, *args, run_func=True):

        if not self.pressed:
            for button in self.master.grid_slaves():
                if button.pressed:
                    button.unpress()
                    break

            self.configure(background='#ECF2F5')
            self.type_func()
            self.pressed = True
            if run_func:
                self.func()
                self.caller_button.press()


    def unpress(self, *args):

        self.configure(background='white')
        self.pressed = False


    def type_func(self, *args):

        if self.type == 'font':
            self.caller_button.configure(text=self.name, font=[self.name, '12'], anchor='center') #Cannot use textvariable because I would need fontvariable too.
        elif self.type == 'size':
            self.caller_button.displayed_size.set(self.name)


class DropdownFont(tk.Frame):

    '''This is used to create a fixed sized field as the font button, because changing the font messes with the
        size of the button that has it as its text.'''

    def __init__(self, *args, func, **kwargs):

    # We initially need a frame to be used with the grid() manager, however, inside, we need to use
    # pack(), because it makes the button nice and proportional in every way. Grid seems to not work properly
    # in this case. So we are creating yet another frame inside, but manage it with pack() manager.
        tk.Frame.__init__(self, *args, width=141, height=38, bg='white', **kwargs)

        self.inside_frame = tk.Frame(self, width=141, height=38, bg='white')

        self.button = ButtonFontSize(self.inside_frame, type_='font', func=func)

        self.inside_frame.pack()
        self.button.pack()
        self.inside_frame.pack_propagate(False)


class ButtonFontSize(ButtonTxt):
# Somehow works. Cheers: https://stackoverflow.com/questions/3085696/adding-a-scrollbar-to-a-group-of-widgets-in-tkinter/3092341#3092341
    def __init__(self, *args, type_, button_entry=None, **kwargs):

        '''Button that uses Popupwindow, frames and buttons to create a scrollable dropdown-like widget.
        It used by both Font and Size options as a dropdown menu.'''

        ButtonTxt.__init__(self, *args, **kwargs)

        self.type = type_
        self.button_entry = button_entry
        # For Size buttons.
        if self.type == 'font':
            self.configure(width=17, height=2)
            self.values = ['Calibri', 'Arial', 'Times New Roman', 'Cambria', 'Georgia', 'Impact', 'Segoe UI', 'Malgun Gothic', 'Comic Sans MS', 'Courier New']
            self.x = lambda: self.winfo_rootx()
            self.y = lambda: self.winfo_rooty() +35
            self.geometry = lambda: f'158x200+{self.x()}+{self.y()}'
        elif self.type == 'size':
            self.displayed_size = self.button_entry.entry.textvar
            self.configure(height=2)
            self.values = [str(n) for n in range(6, 16)] + [str(n) for n in range(16, 30, 2)]
            self.x = lambda: self.button_entry.winfo_rootx()
            self.y = lambda: self.button_entry.winfo_rooty() +35
            self.geometry = lambda: f'50x260+{self.x()}+{self.y()}'

        self.dropdown = PopupWindow(root)
        self.frame = tk.Frame(self.dropdown)
        self.frame.pack()

        self.canvas = tk.Canvas(self.frame)
        self.menu = tk.Frame(self.canvas)
        self.scrollbar = ttk.Scrollbar(self.frame, orient='vertical',
            command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.scrollbar.set)

        self.scrollbar.pack(side="right", fill="y")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas.create_window((0, 0), window=self.menu, anchor="nw")
        self.populate_menu()

        root.update()
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))
        self.dropdown.bind("<MouseWheel>", lambda e: self.canvas.yview_scroll(int(-1*(e.delta/120)), "units"))
        root.bind('<Configure>', lambda e: self.dropdown.geometry(self.geometry()), "+")
        # Attaching the window to the root screen.


    def press(self, *args):

        if self.pressed:
            self.dropdown.state('withdrawn')
            self.dropdown.close()
            self.configure(background='white', foreground='black')
            if self.type == 'size':
                self.button_entry.press_cosmetic()
            self.pressed = False
        else:
            self.dropdown.lift()
            self.dropdown.state('normal')
            self.dropdown.geometry(self.geometry())
            self.dropdown.close(self.press, self)
            self.configure(background='#ECF2F5', foreground='#0457B6')
            if self.type == 'size':
                self.button_entry.press_cosmetic()
            self.pressed = True


    def get(self, *args):

        for button in self.menu.grid_slaves():
            if button.pressed:
                return button.name
        return self.displayed_size.get()
        # If unable to find a pressed button, it means value is typed
        # in manually via size entry widget. 


    def set(self, arg):

        for button in self.menu.grid_slaves():
            if button.name == arg:
                button.press(run_func=False)
                return None
        # If unable to find an active button with the code above, we 
        # continue to other scenarios.
        if self.type == 'font':
            self.configure(text='Font', font='Calibri 12 italic')
        elif self.type == 'size':
            if arg == 'multiple':
                self.displayed_size.set('')
            else:
                self.displayed_size.set(arg)
        for button in self.menu.grid_slaves():
            if button.pressed:
                button.unpress()


    def populate_menu(self, *args):

        for n, value in enumerate(self.values):
            item = ButtonMenu(self.menu, name=value, caller_button=self, type_=self.type, text=value, anchor='w', func=self.func)
            item.grid(row=n, sticky='WE')


class DropdownSize(tk.Frame):

    '''This is used to create a fixed sized field, same as in the DropdownFont. However this is even more extended
    as it contains an entry together with a dropdown and they all communicate with each other.'''

    def __init__(self, *args, func, textwidget, **kwargs):
    # We initially need a frame to be used with the grid() manager, however, inside, we need to use
    # pack(), because it makes the button nice and proportional in every way. Grid seems to not work properly
    # in this case. So we are creating yet another frame inside, but manage it with pack() manager.
        tk.Frame.__init__(self, *args, width=50, height=38, bg='white', **kwargs)

        self.inside_frame = tk.Frame(self, width=50, height=38, bg='white')
        self.inside_frame.pack()

        self.popup = PopupWindow(root, background='#ECF2F5')
        self.entry = EntrySize(self.popup, func=func, textwidget=textwidget)
        self.button_entry = EntrySizeButton(self.inside_frame, entry_w=self.popup, entry=self.entry, textvariable=self.entry.textvar, func=func, font='Calibri 13')
        self.button_dropdown = ButtonFontSize(self.inside_frame, text=u"\u25BC", type_='size', button_entry=self.button_entry, func=func)
        self.entry.dropdown = self.button_dropdown

        self.button_entry.pack(side='left')
        self.button_dropdown.pack(side='left')
        self.inside_frame.pack_propagate(False)
        self.entry.pack(expand=True)
    # The popup with an entry inside it will appear on top of the button_entry, when the button will
    # be pressed. The button itself stands more like a Label - to show the current size.


class EntrySizeButton(ButtonTxt):

    '''The button that after being pressed turns into an entry for user to type in the prefered letter size.'''

    def __init__(self, master, *args, entry, entry_w, **kwargs):
        ButtonTxt.__init__(self, master, *args, **kwargs)

        self.configure(height=2)

        self.master = master
        self.entry = entry
        self.entry_w = entry_w
        self.geometry = lambda: f'{self.winfo_width()}x{self.winfo_height()}+{self.winfo_rootx()}+{self.winfo_rooty()}'
        root.bind('<Configure>', lambda e: self.entry_w.geometry(self.geometry()), "+")


    def press(self, *args):

        self.entry_w.lift()
        self.entry_w.state('normal')
        self.entry_w.geometry(self.geometry())
        self.entry.initiate()


    def press_cosmetic(self, *args):
    # For when the Size arrow is pressed, that this button would look
    # pressed as well.
        if self.pressed:
            self.configure(background='white', foreground='black')
            self.pressed = False
        else:
            self.configure(background='#ECF2F5', foreground='#0457B6')
            self.pressed = True


class EntrySize(tk.Entry):

    '''The entry that appears on top of the EntrySizeButton after it is pressed and lets the user to type in the
    prefered letter size. It hides again after the successful entry,'''

    def __init__(self, master, *args, func, textwidget, **kwargs):
        tk.Entry.__init__(self, master, *args, width=3, relief='flat', 
            highlightcolor=None, highlighbackground=None, highlightthickness=0,
            background='#ECF2F5', exportselection=False, borderwidth=0, 
            font='Calibri 13', justify='center', **kwargs)

        self.master = master
        self.textwidget = textwidget
        self.dropdown = None
        self.func = func
        self.textvar = tk.StringVar()
        self.configure(textvariable=self.textvar, state='disabled')

        self.bind('<Return>', self.validate)
        self.bind('<Escape>', lambda e: self.validate('escape'))


    def initiate(self, *args):

        self.master.close(self.validate, self)
        self.configure(state='normal')
        self.focus()
        self.selection_range(0, 'end')
        self.current = self.get()


    def validate(self, *args):

        size = self.get()

        try:
            if len(size) < 3 and int(size) > 0 and 'escape' not in args:
                self.dropdown.set(size)
                self.func()
                self.closing()
            else:
                raise ValueError
        except:
            self.textvar.set(self.current)
            self.closing()


    def closing(self, *args):

        self.textwidget.focus()
        self.configure(state='disabled')
        self.master.state('withdrawn')
        self.master.close()


########### ButtonImg subclasses.
class ButtonListing(ButtonImg):

    '''Subclass of ButtonImg used for bulleting and numbering list buttons.'''

    def __init__(self, *args, **kwargs):
        ButtonImg.__init__(self, *args, **kwargs)

        self.bind('<ButtonRelease-1>', lambda e: self.configure(image=self.img2))
        # Listing buttons do not stay pressed when pressed, thus they need to
        # revert back to img2 after pressing.


    def press(self, *args):
        self.func()


class ButtonJust(ButtonImg):

    '''A class for text justification buttons which all three are interconnected.'''

    def __init__(self, *args, **kwargs):
        ButtonImg.__init__(self, *args, **kwargs)


    def otherbuttons(self, otherbutton1, otherbutton2):
    # Need to put this as a seprate function because if I would put it as an initial
    # argument, I would get an Attribute error when initialising first two Justification buttons.
        self.otherbutton1 = otherbutton1
        self.otherbutton2 = otherbutton2


    def press(self, *args, run_func=True):
    # Only one of the three buttons can be pressed at a time.
        if run_func:
            self.func()

        if not self.pressed:
            self.configure(background='#ECF2F5', image=self.img3)
            self.pressed = True

            self.otherbutton1.unpress()
            self.otherbutton2.unpress()


    def unpress(self, *args):

        self.configure(background='white', image=self.img1)
        self.pressed = False


class ButtonSpacing(ButtonImg):

    '''A joint class both for the line spacing and paragraph spacing buttons. It is the same dropdown
    as the one used in font/size, just that it is built on ButtonImg as it needs to use an image.'''

    def __init__(self, *args, type_, **kwargs):
        ButtonImg.__init__(self, *args, **kwargs)

        self.type = type_
        if self.type == 'lines':
            self.values = {'Single': '0', '1.15': '1.5', '1.5': '2', 'Double': '4'}
        elif self.type == 'paragraphs':
            self.values = {'None': '0', 'Small': '1.5', 'Medium': '3', 'Huge': '5'}
        self.x = lambda: self.winfo_rootx()
        self.y = lambda: self.winfo_rooty() +35
        self.geometry = lambda: f'65x116+{self.x()}+{self.y()}'

        self.dropdown = PopupWindow(root)
        self.menu = tk.Frame(self.dropdown)
        self.menu.pack()

        self.populate_menu()

        root.update()
        root.bind('<Configure>', lambda e: self.dropdown.geometry(self.geometry()), "+")
        # Attaching the window to the root screen.


    def press(self, *args):

        if self.pressed:
            self.dropdown.state('withdrawn')
            self.dropdown.close()
            self.configure(background='white', image=self.img1)
            self.pressed = False
        else:
            self.dropdown.lift()
            self.dropdown.state('normal')
            self.dropdown.geometry(self.geometry())
            self.dropdown.close(self.press, self)
            self.configure(background='#ECF2F5', image=self.img3)
            self.pressed = True


    def get(self, *args):

        for button in self.menu.grid_slaves():
            if button.pressed:
                return button.name


    def set(self, arg):
        for button in self.menu.grid_slaves():
            if button.name == arg:
                button.press(run_func=False)
                return None
        # If unable to find an active button with the code above, we 
        # continue to the selection scenario.
        for button in self.menu.grid_slaves():
            if button.pressed:
                button.unpress()
        

    def populate_menu(self, *args):

        for n, value in enumerate(self.values):
            item = ButtonMenu(self.menu, name=self.values[value], caller_button=self, text=value, anchor='w', func=self.func)
            item.grid(row=n, sticky='WE')


########### Coloring buttons.
class ButtonColor(ButtonTxt):

    '''The two coloring buttons are very similar, just that ones is built on ButtonTxt, the other on ButtonImg classes.
    They are both dropdowns that dropdown a canvas with a color pallete.'''

    def __init__(self, *args, **kwargs):
        ButtonTxt.__init__(self, *args, **kwargs)

        self.x = lambda: self.winfo_rootx()
        self.y = lambda: self.winfo_rooty() +35
        self.geometry = lambda: f'130x107+{self.x()}+{self.y()}'

        self.dropdown = PopupWindow(root)
        self.canvas = ColorTable(self.dropdown, master=self, func=self.func)
        self.canvas.pack()

        root.update()
        root.bind('<Configure>', lambda e: self.dropdown.geometry(self.geometry()), "+")
        # Attaching the window to the root screen.


    def press(self, *args):

        if self.pressed:
            self.dropdown.state('withdrawn')
            self.dropdown.close()
            self.configure(background='white', foreground=self.get())
            self.pressed = False
        else:
            self.dropdown.lift()
            self.dropdown.state('normal')
            self.dropdown.geometry(self.geometry())
            self.dropdown.close(self.press, self)
            self.configure(background='#ECF2F5', foreground='#0457B6')
            self.pressed = True


    def get(self, *args):

        for tag in self.canvas.gettags('pressed'):
            if tag.startswith('#'):
                return tag


    def set(self, color=None):

        self.canvas.set_color(color=color)


class ColorTable(tk.Canvas):

    '''The color pallete that appears in a dropdown of coloring buttons.'''

    def __init__(self, *args, master, func, highlight=False, **kwargs):
        tk.Canvas.__init__(self, *args, background='white', cursor='hand2', **kwargs)

        self.master = master
        self.func = func
        self.highlight = highlight

        colors = iter(
            [
            '#000000', '#222222', '#808080', '#D3D3D3', '#FFFFFF',
            '#800020', '#FF0000', '#FFA500', '#FFFF00', '#00FF00', 
            '#4B0082', '#8B0000', '#EE7600', '#CCCC00', '#006400',
            '#00FFFF', '#6495ED', '#0000FF', '#800080', '#FF00FF',
                ])
        x1 = 5
        x2 = 25
        y1 = 5
        y2 = 25
        for i in range(20):
            if i % 5 == 0 and i != 0:
                x1 = 5
                x2 = 25
                y1 += 25
                y2 += 25
            color = next(colors)
            Rectangle(self, x1, y1, x2, y2, color)
            x1 += 25
            x2 += 25

        if self.highlight:
            self.create_window(65, 120, window=ButtonCanvas(self, text='None', font=['Calibri', '11'], func=lambda: self.set_color(run_func=True)))


    def set_color(self, color=None, run_func=False):

        '''Visually marks the selected rectangle and runs the function for the text widget.'''

        try:
        # There is no color 'pressed' until the very first cursor() func fires
        # and sets the color so this part needs to be skipped.
            currently_pressed = self.find_withtag('pressed')
            x1, y1, x2, y2 = self.coords(currently_pressed)
            x1 += 3
            y1 += 3
            x2 -= 3
            y2 -= 3
            self.coords(currently_pressed, x1, y1, x2, y2)
            self.dtag(currently_pressed, 'pressed')
        except:
            pass

        if color:
            newly_pressed = self.find_withtag(color)
            x1, y1, x2, y2 = self.coords(newly_pressed)
            x1 -= 3
            y1 -= 3
            x2 += 3
            y2 += 3
            self.coords(newly_pressed, x1, y1, x2, y2)
            self.addtag_withtag('pressed', newly_pressed)
            if self.highlight:
                self.master.change_images(color)
                self.master.configure(image=self.master.img1)
            else:
                self.master.configure(foreground=color)
        else:
        # If there is no color passed into the function, it means the selection is happenning
        # and multiple colors fall into it or user is setting a color to "None" if the class type is "highlight".
            if self.highlight:
                self.master.change_images()
                self.master.configure(image=self.master.img1)
            else:
                self.master.configure(foreground='black')

        if run_func:
            self.func(color)
            self.master.press()
            

class Rectangle:

    '''The rectangle Canvas item that acts as a button in a color palette.'''

    def __init__(self, canvas, x1, y1, x2, y2, color):
        
        canvas.create_rectangle(x1, y1, x2, y2, fill=color, tags=color, activeoutline=color, activewidth=6)
        
        self.canvas = canvas
        self.color = color
        self.id = self.canvas.find_withtag('all')[-1]
        
        canvas.tag_bind(self.id, '<ButtonRelease-1>', self.press, "+")


    def press(self, event):

        if self.canvas.find_closest(self.canvas.canvasx(event.x), self.canvas.canvasy(event.y))[0] == self.id:
        # This makes the <ButtonRelease> event work the same way as on the Button widget.
        # Automatic "current" tag does not do the job as it freezes on one item when the event happens.             
            self.canvas.set_color(self.color, run_func=True)


class ButtonCanvas(ButtonTxt):

    '''The "None" button in the highlight dropdown palette.'''

    # I could not get away with not creating a separate class for this button.
    def __init__(self, *args, **kwargs):
        ButtonTxt.__init__(self, *args, **kwargs)

        self.configure(height=1, width=14)

    def press(self, *args):
        self.func()


class ButtonHighlight(ButtonImg):

    '''Basically the same as ButtonColor, just has an additional "None" button in the dropdown and
    uses images with different colors depending on the highlight color chosen.'''

    def __init__(self, *args, color_dict, **kwargs):
        ButtonImg.__init__(self, *args, **kwargs)

        self.color_dict = color_dict

        self.x = lambda: self.winfo_rootx()
        self.y = lambda: self.winfo_rooty() +35
        self.geometry = lambda: f'131x140+{self.x()}+{self.y()}'

        self.dropdown = PopupWindow(root)
        self.canvas = ColorTable(self.dropdown, master=self, func=self.func, highlight=True)
        self.canvas.pack()

        root.update()
        root.bind('<Configure>', lambda e: self.dropdown.geometry(self.geometry()), "+")
        # Attaching the window to the root screen.


    def press(self, *args):

        if self.pressed:
            self.dropdown.state('withdrawn')
            self.dropdown.close()
            self.configure(background='white', image=self.img1)
            self.pressed = False
        else:
            self.dropdown.lift()
            self.dropdown.state('normal')
            self.dropdown.geometry(self.geometry())
            self.dropdown.close(self.press, self)
            self.configure(background='#ECF2F5', image=self.img3)
            self.pressed = True


    def get(self, *args):

        for tag in self.canvas.gettags('pressed'):
            if tag.startswith('#'):
                return tag


    def set(self, color=None):

        self.canvas.set_color(color=color)


    def change_images(self, color=None):

        if not color:
            color = 'NoColor'

        images = self.color_dict[color]
        self.img1 = images[0]
        self.img2 = images[1]
        self.img3 = images[2]


############################### OTHER CLASSES.
class WordDoc():

    '''This class converts the text written in the application's text field to a Microsoft Word document and saves it
    in the given directory.'''

    def __init__(self, text, directory, all_tag_configs):

        self.text = text
        self.all_tag_configs = all_tag_configs
        self.p_configs = [config for config in self.all_tag_configs if config not in ('font', 'underline', 'foreground', 'background')]
        self.r_configs = [config for config in self.all_tag_configs if config not in self.p_configs]
        self.highlight_map = {
            '#000000': docx.enum.text.WD_COLOR_INDEX.BLACK, '#222222': docx.enum.text.WD_COLOR_INDEX.BLACK,
            '#808080': docx.enum.text.WD_COLOR_INDEX.GRAY_50, '#D3D3D3': docx.enum.text.WD_COLOR_INDEX.GRAY_25,
            '#FFFFFF': docx.enum.text.WD_COLOR_INDEX.WHITE, '#800020': docx.enum.text.WD_COLOR_INDEX.DARK_RED,
            '#FF0000': docx.enum.text.WD_COLOR_INDEX.RED, '#FFA500': docx.enum.text.WD_COLOR_INDEX.YELLOW,
            '#FFFF00': docx.enum.text.WD_COLOR_INDEX.YELLOW, '#00FF00': docx.enum.text.WD_COLOR_INDEX.BRIGHT_GREEN,
            '#4B0082': docx.enum.text.WD_COLOR_INDEX.DARK_BLUE, '#8B0000': docx.enum.text.WD_COLOR_INDEX.DARK_RED,
            '#EE7600': docx.enum.text.WD_COLOR_INDEX.DARK_YELLOW, '#CCCC00': docx.enum.text.WD_COLOR_INDEX.YELLOW,
            '#006400': docx.enum.text.WD_COLOR_INDEX.GREEN, '#00FFFF': docx.enum.text.WD_COLOR_INDEX.TURQUOISE,
            '#6495ED': docx.enum.text.WD_COLOR_INDEX.TEAL, '#0000FF': docx.enum.text.WD_COLOR_INDEX.BLUE,
            '#800080': docx.enum.text.WD_COLOR_INDEX.VIOLET, '#FF00FF': docx.enum.text.WD_COLOR_INDEX.PINK
        }  # I have more colors than available for highlighting via python-docx.
        self.word_dic = {}

        self.dump_runs()
        self.document = docx.Document()
        self.create_doc()
        self.document.save(directory)



    def dump_runs(self, *args):

        '''Populate the word_dic with paragraphs, runs and their styling configurations.'''

        last_tag = ''
        last_p = '1'
        self.word_dic[last_p] = {}
        r = ''
        for n, ch in enumerate(self.text.get('1.0', 'end')):
            ch_indx = '1.0' + f'+{n}c'

            p = self.text.index(ch_indx).split('.')[0]
            change_p = False
            if p != last_p:
                change_p = True

            change_t = False
            for i in self.text.tag_names(ch_indx):
                if i.startswith('tag'):
                    tag = i
                    if tag != last_tag:
                        change_t = True

            if change_p or change_t:
            # If the new paragraph or tag starts, we end a current run and start a new one.
                if r != '':
                    begin_r = self.word_dic[last_p][r]['text']
                    self.word_dic[last_p][r]['text'] = self.text.get(begin_r, ch_indx)
                    self.word_dic[last_p][r]['configs'] = {}
                    
                    r_configs = self.word_dic[last_p][r]['configs']
                    self.save_configs('r', r_configs, last_tag)
                
                if change_p:
                    self.word_dic[p] = {}
                    last_p = p

                r = self.give_run()
                self.word_dic[last_p][r] = {}
                self.word_dic[last_p][r]['text']  = ch_indx

                if change_t:
                    last_tag = tag

        begin_r = self.word_dic[last_p][r]['text']
        self.word_dic[last_p][r]['text'] = self.text.get(begin_r, ch_indx)
        if self.word_dic[last_p][r]['text'] != '':
            self.word_dic[last_p][r]['configs'] = {}
            r_configs = self.word_dic[last_p][r]['configs']
            self.save_configs('r', r_configs, last_tag)
        else:
            del self.word_dic[last_p][r]
        # There's a weird-who knows from where-empty string at the very end which applies as a "run".
        # I could just probably straight up delete it, but just to be sure, I do a little check if its really empty before deleting it.
        
        for p in self.word_dic:
        # Saving configurations for each paragraph.
            self.word_dic[p]['configs'] = {}
            p_configs = self.word_dic[p]['configs']
            ch_indx = str(p)+'.0'
            for i in self.text.tag_names(ch_indx):
                if i.startswith('tag'):
                    tag = i
            self.save_configs('p', p_configs, tag, ch_indx=ch_indx)


    def give_run(self, *args):

        run = 1
        for d in self.word_dic.values():
            for r in d:
                if r != 'configs':
                    if int(r[3:]) == run:
                        run += 1

        return 'run' + str(run)


    def save_configs(self, p_or_r, path, tag, ch_indx=None):

        '''Enters the paragraph and run configs into the word_dic.'''

        if p_or_r == 'p':
            p_configs = path

            for tagconfig in self.p_configs:
                config = self.text.tag_cget(tag, tagconfig)
                if tagconfig == 'justify':
                    if config == 'left':
                        p_configs['justify'] = docx.enum.text.WD_ALIGN_PARAGRAPH.LEFT
                    elif config == 'center':
                        p_configs['justify'] = docx.enum.text.WD_ALIGN_PARAGRAPH.CENTER
                    elif config == 'right':
                        p_configs['justify'] = docx.enum.text.WD_ALIGN_PARAGRAPH.RIGHT
                if tagconfig == 'spacing2':
                    if config == '0':
                        p_configs['line_spacing'] = None
                    elif config == '1.5':
                        p_configs['line_spacing'] = 1.5
                    elif config == '2':
                        p_configs['line_spacing'] = 2.0
                    elif config == '4':
                        p_configs['line_spacing'] = 2.5
                if tagconfig == 'spacing1':
                    if config == '0':
                        p_configs['p_spacing'] = None
                    elif config == '1.5':
                        p_configs['p_spacing'] = docx.shared.Pt(10)
                    elif config == '3':
                        p_configs['p_spacing'] = docx.shared.Pt(15)
                    elif config == '5':
                        p_configs['p_spacing'] = docx.shared.Pt(20)
                if tagconfig == 'lmargin1':
                    if config != '0':
                        listing_ch = self.text.get(self.text.index(ch_indx +' +1c'))
                        if  listing_ch == '•' or listing_ch in [num for num in '0123456789']:
                            p_configs['indent'] = docx.shared.Cm(int(config[0]))
                        else:
                            p_configs['indent'] = None
                    else:
                        p_configs['indent'] = None  

        elif p_or_r == 'r':
            r_configs = path

            for tagconfig in self.r_configs:
                config = self.text.tag_cget(tag, tagconfig)
                if tagconfig == 'font':
                    font = font_unpack(config)
                    r_configs['font'] = font[0]
                    r_configs['size'] = docx.shared.Pt(int(font[1]))
                    if 'bold' in font:
                        r_configs['bold'] = True
                    else:
                        r_configs['bold'] = False
                    if 'italic' in font:
                        r_configs['italic'] = True
                    else:
                        r_configs['italic'] = False
                if tagconfig == 'underline':
                    if config == '1':
                        r_configs['underline'] = True
                    else:
                        r_configs['underline'] = None
                if tagconfig == 'foreground':
                    R = int(config[1:3], 16)
                    G = int(config[3:5], 16)
                    B = int(config[5:], 16)
                    r_configs['color'] = docx.shared.RGBColor(R, G, B)
                if tagconfig == 'background':
                    if config:
                        r_configs['highlight'] = self.highlight_map[config]
                    else:
                        r_configs['highlight'] = None


    def create_doc(self, *args):

        '''Enters the text and its styling configurations into a Word file.'''

        for paragraph in self.word_dic:

            p_configs = self.word_dic[paragraph]['configs']
            para = self.document.add_paragraph()
            p = para.paragraph_format

            p.alignment = p_configs['justify']
            p.line_spacing = p_configs['line_spacing']
            p.space_before = p_configs['p_spacing']
            p.space_after = p_configs['p_spacing']
            p.left_indent = p_configs['indent']

            for run in self.word_dic[paragraph]:
                if run != 'configs':
                    r_path = self.word_dic[paragraph][run]
                    r_configs = r_path['configs']
                    r = para.add_run(r_path['text'])

                    font = r.font
                    font.name = r_configs['font']
                    font.size = r_configs['size']
                    font.bold = r_configs['bold']
                    font.italic = r_configs['italic']
                    font.underline = r_configs['underline']
                    font.color.rgb = r_configs['color']
                    font.highlight_color = r_configs['highlight']


def font_unpack(font_raw):

    '''Strings for font type that are more than one word long in Tk
    are enclosed with {} so we need to unpack that. This function is put down here
    as a "global" function because it is also used by the WordDoc class.'''

    if '{' in font_raw:
        font = [font_raw.split('}')[0][1:]] + font_raw.split('}')[1].split()
    else:
        font = font_raw.split()
    return font         


if __name__ == "__main__":
    root = tk.Tk()
    root.title("Student's Text Editor")
    root.iconphoto(True, PhotoImage(file="./icon.png"))
    application = Application(root)
    application.populate_subjects()

    root.mainloop()