# StudentTextEditor

Welcome to the Student's Text Editor!

This application is created for Windows only.

You only need to run the StudentsTextEditor.py file.

In order to run the code, besides Python 3, you will need to install "pyperclip" and "docx" modules.


Use the terminal and type in the following commands:

pip install pyperclip

pip install python-docx


Have a wonderful day!
